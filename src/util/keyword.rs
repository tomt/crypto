//! The [Keyword] key type (just an array of `i32`).

use std::ops::Index;

use super::combinators::{Period, PeriodIdx};
use crate::prelude::*;

/// A keyword for a cipher.
#[derive(Debug, Clone, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Keyword(pub Msg);

impl Keyword {
    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    pub fn period(&self) -> Period {
        Period(self.len())
    }
}

impl Identity for Keyword {
    fn identity(ctx: &Context) -> Self {
        Keyword(Msg {
            alphabet_len: ctx.alphabet_len,
            data: vec![0],
        })
    }
}

impl Index<PeriodIdx> for Keyword {
    type Output = i32;

    fn index(&self, PeriodIdx(index): PeriodIdx) -> &Self::Output {
        &self.0[index]
    }
}

impl<'kw> IntoIterator for &'kw Keyword {
    type Item = i32;
    type IntoIter = std::iter::Copied<std::iter::Cycle<std::slice::Iter<'kw, i32>>>;

    fn into_iter(self) -> Self::IntoIter {
        let Msg { data, .. } = &self.0;
        data.as_slice().iter().cycle().copied()
    }
}
