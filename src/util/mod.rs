//! Data types and algorithms that provide primitives for
//! building ciphers and analysers.

pub mod alphabet;
pub mod combinators;
pub mod keyword;
pub mod matrix;
pub mod num;
pub mod primes;
pub mod stats;
pub mod text;
pub mod vector;

use std::{iter::Sum, ops::Mul};

pub use alphabet::Alphabet;
pub use keyword::Keyword;
pub use matrix::{Matrix, SquareMatrix};
pub use num::{Float, Integer, ModularInverse, Num, NumIndex, One, Signed, Zero};
pub use primes::Primes;
pub use vector::Vector;

/// Implemented by types with a natural inverse, for
/// matrices this would be the inverse matrix, and
/// for ciphers this would be the inverse (decryption)
/// key.
pub trait Inverse {
    /// Returns the inverse of the value.
    fn inverse(self) -> Self;
}

/// Takes a `Vec` v, and a period `n`, and produces a `Vec`
/// containing `n` entries, with the `i`th entry containing
/// the entries at i, i + n, i + 2n, i + 3n, ... in the input
/// `v`.
pub fn divide<T>(n: usize, v: Vec<T>) -> Vec<Vec<T>> {
    let mut out: Vec<Vec<T>> = (0..n).map(|_| Vec::with_capacity(v.len() / n)).collect();

    for (index, el) in v.into_iter().enumerate() {
        out[index % n].push(el);
    }

    out
}

/// Takes a `Vec` of `Vec`s and produces a `Vec` containing
/// all input elements, taking one element from each `Vec`
/// in the input list in turn.
pub fn combine<T>(v: Vec<Vec<T>>) -> Vec<T> {
    let count = v.len();
    let len = v.iter().map(|v| v.len()).sum();

    let mut i = 0;
    let mut out = Vec::with_capacity(len);
    let mut iters = v.into_iter().map(Vec::into_iter).collect::<Vec<_>>();

    while out.len() < len {
        out.extend(iters[i % count].next());
        i += 1;
    }

    out
}

/// Checks whether an integer is even.
pub fn even<T: Integer>(n: T) -> bool {
    n % (T::ONE + T::ONE) == T::ZERO
}

/// Checks whether an integer is odd.
pub fn odd<T: Integer>(n: T) -> bool {
    !even(n)
}

/// Returns a vector of size `alphabet_len` containing all the values in the
/// range `0..alphabet_len`, keyed by the slice `key`.  
///
/// # Detail
/// The alphabet is structured as follows:  \
///     `KEY + (KEY.last()..alphabet_len) + (0..KEY.last())` \
/// with any repeated letters discarded.
///
/// However, since repetitions are discarded, KEY.last() is the last non-repeated
/// letter in the KEY, not necessarily the final value in the slice.
///
/// # Example
///
/// ```
/// # use classic_crypto::util;
/// let key = [7, 4, 11, 11, 14]; // 'hello' with english `Lang`
/// let alphabet_len = 26;
/// let expected = vec![
///      7,  4, 11, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
///     24, 25,  0,  1,  2,  3,  5,  6,  8,  9, 10, 12, 13,
/// ];
/// assert_eq!(
///     util::fill_continue(&key, alphabet_len),
///     expected
/// );
/// ```
pub fn fill_continue(key: &[i32], alphabet_len: usize) -> Vec<i32> {
    let mut alphabet = Vec::with_capacity(alphabet_len);

    key.iter().copied().for_each(|i| {
        if !alphabet.contains(&i) {
            alphabet.push(i);
        }
    });
    let pivot = *alphabet.last().unwrap_or(&0);
    (pivot..alphabet_len as i32).chain(0..pivot).for_each(|i| {
        if !alphabet.contains(&i) {
            alphabet.push(i);
        }
    });

    alphabet
}

/// Returns a vector of size `alphabet_len` containing all the values in the
/// range `0..alphabet_len`, keyed by the slice `key`.  
///
/// # Detail
///
/// The alphabet is structured as follows: \
///     `KEY + (0..alphabet_len)` \
/// with any repeated letters discarded.
///
/// # Example
///
/// ```
/// # use classic_crypto::util;
/// let key = [7, 4, 11, 11, 14]; // 'hello' with english Language
/// let alphabet_len = 26;
/// let expected = vec![
///      7,  4, 11, 14,  0,  1,  2,  3,  5,  6,  8,  9, 10,
///     12, 13, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
/// ];
/// assert_eq!(
///     util::fill_start(&key, alphabet_len),
///     expected
/// );
/// ```
pub fn fill_start(key: &[i32], alphabet_len: usize) -> Vec<i32> {
    let mut alphabet = Vec::with_capacity(alphabet_len);

    key.iter()
        .copied()
        .chain(0..alphabet_len as i32)
        .for_each(|i| {
            if !alphabet.contains(&i) {
                alphabet.push(i);
            }
        });

    alphabet
}

/// Returns a vector of size `alphabet_len` containing all the values in the
/// range `0..alphabet_len`, keyed by the slice `key`.
///
/// # Detail
/// The alphabet is structured as follows:  \
///     `KEY + (KEY.max()..alphabet_len) + (0..KEY.last())` \
/// with repeated letters discarded.
///
/// # Example
///
/// ```
/// # use classic_crypto::util;
/// let key = [19, 17, 0, 8, 13]; // 'train' with english `Lang`
/// let alphabet_len = 26;
/// let expected = vec![
///     19, 17,  0,  8, 13, 20, 21, 22, 23, 24, 25,  1,  2,
///      3,  4,  5,  6,  7,  9, 10, 11, 12, 14, 15, 16, 18,
/// ];
/// assert_eq!(
///     util::fill_continue_max(&key, alphabet_len),
///     expected
/// );
/// ```
pub fn fill_continue_max(key: &[i32], alphabet_len: usize) -> Vec<i32> {
    let max = key.iter().max().copied().unwrap_or_default();
    let mut alphabet = Vec::with_capacity(alphabet_len);

    key.iter().copied().for_each(|i| {
        if !alphabet.contains(&i) {
            alphabet.push(i);
        }
    });
    (max..alphabet_len as i32).chain(0..max).for_each(|i| {
        if !alphabet.contains(&i) {
            alphabet.push(i);
        }
    });

    alphabet
}

/// Inverts a substitution alphabet (a `&[i32]` containing each of the
/// elements `0..arr.len()`), so that each element can be indexed in O(1) time.
/// i.e. transforms `arr[idx] = value` to `arr[value] = idx`.
///
/// # Example
///
/// ```
/// # use classic_crypto::util;
/// let arr = [2, 1, 3, 0, 4];
/// let inv = util::invert(&arr);
///
/// let find_index = |elem| arr.iter().position(|&x| x == elem).unwrap();
///
/// assert_eq!(inv[0], find_index(0));
/// assert_eq!(inv[1], find_index(1));
/// assert_eq!(inv[2], find_index(2));
/// assert_eq!(inv[3], find_index(3));
/// assert_eq!(inv[4], find_index(4));
/// ```
pub fn invert<T: NumIndex>(arr: &[T]) -> Vec<T> {
    let mut inv: Vec<_> = vec![T::ZERO; arr.len()];
    invert_into(arr, &mut inv);
    inv
}

/// Inverts the data in `key`, storing the result in `inv`.
/// See the docs for [`invert`].
pub fn invert_into<T: NumIndex>(arr: &[T], inv: &mut Vec<T>) {
    inv.resize(arr.len(), T::ZERO);

    for (idx, elem) in arr.iter().copied().enumerate() {
        let k = elem.as_usize();
        let v = T::from_usize(idx);

        inv[k] = v;
    }
}

/// Generates a permutation of the range `0..arr.len()` sorted in the same order
/// as `arr`, representing the current permutation in index form.
///
/// # Detail
///
/// The order array contains a `0` where the lowest element was, and `arr.len()-1`
/// where the maximum element (and furthest right) was. Equal elements in the array
/// are ordered consecutively from left to right.
///
/// # Example
///
/// ```
/// # use classic_crypto::util;
/// let key = vec![7, 4, 11, 11, 14]; // 'hello' with english Language
/// let order = util::order(&key);
/// assert_eq!(order, vec![1, 0, 2, 3, 4]);
/// ```
pub fn order<T: Num>(key: &[T]) -> Vec<usize> {
    let mut order = vec![0; key.len()];
    let mut pos = 0;
    let mut idx = T::ZERO;

    while pos < key.len() {
        for i in 0..key.len() {
            if key[i] == idx {
                order[i] = pos;
                pos += 1;
            }
        }
        idx += T::ONE;
    }

    order
}

/// Finds the composition of two permutations of equal length.
/// The permutations, of common length N, must contain each integer
/// in the range 0..N (not including N), in some permutation.
/// The result of this function is a new permutation, of length N,
/// which is equivalent to applying `b`, then `a`.
///
/// # Panics
///
/// If the two permutations differ in length.
///
/// # Examples
///
/// ```rust
/// # use classic_crypto::util;
///
/// let a = [2, 1, 0];
/// let b = [1, 2, 0];
///
/// assert_eq!(&util::compose(&a, &b), &[1, 0, 2]);
/// ```
pub fn compose<T: NumIndex>(a: &[T], b: &[T]) -> Vec<T> {
    assert_eq!(a.len(), b.len(), "Permutations must have equal length");

    b.iter().map(|&b| a[b.as_usize()]).collect()
}

/// Calculates the pattern of a word, by assigning the same numerical value
/// to identical values, always starting from the first, not the lowest.
/// If a word was encrypted with two different monoalphabetic substitutions,
/// their patterns would remain the same.
///
/// # Example
///
/// ```rust
/// # use classic_crypto::util;
/// let word = [7, 4, 11, 11, 14];
/// assert_eq!(&util::pattern(&word), &[0, 1, 2, 2, 3]);
/// ```
pub fn pattern<T: PartialEq<T>>(word: &[T]) -> Vec<usize> {
    let mut pattern = vec![usize::MAX; word.len()];
    let mut n = 0;

    for idx in 0..word.len() {
        if pattern[idx] == usize::MAX {
            pattern[idx] = n;

            for i in idx + 1..word.len() {
                if word[i] == word[idx] {
                    pattern[i] = n;
                }
            }

            n += 1;
        }
    }

    pattern
}

/// Calculates the modular multiplicative inverse (`mmi`) of `a` modulo `b`.
/// `mmi` satisfies `(a * mmi) % b == 1`. Returns `None` if there is no solution.
///
/// ```
/// # use classic_crypto::util;
/// let a = 11;
/// let b = 26;
/// let mmi = util::mmi(a, b).unwrap();
/// assert_eq!((mmi * a) % b, 1);
/// ```
pub fn mmi<T: Integer>(a: T, b: T) -> Option<T> {
    let (g, x, _) = extended_gcd(a, b);
    (g == T::ONE).then(|| modulo(x, b))
}

/// Performs the extended euclidean algorithm on `a` and `b`.
#[allow(clippy::many_single_char_names)]
pub fn extended_gcd<T: Integer>(a: T, b: T) -> (T, T, T) {
    if a == T::ZERO {
        (b, T::ZERO, T::ONE)
    } else {
        let (g, x, y) = extended_gcd(b % a, a);
        (g, y - (b / a) * x, x)
    }
}

/// Calculates the greatest common divisor of `a` and `b`.
///
/// # Example
///
/// ```
/// # use classic_crypto::util;
/// let a = 24;
/// let b = 32;
/// assert_eq!(util::gcd(a, b), 8);
/// ```
pub fn gcd<T: Integer>(a: T, b: T) -> T {
    if b == T::ZERO {
        a
    } else {
        gcd(b, a % b)
    }
}

/// Performs the modulo operation, but ensures a positive result,
/// so that it functions for negative numbers by wrapping around
/// the modulus `b`.
///
/// # Example
///
/// ```
/// # use classic_crypto::util;
/// let a = -10;
/// let b = 26;
/// assert_eq!(util::modulo(a, b), 16);
/// ```
pub fn modulo<T: Integer>(a: T, b: T) -> T {
    (b + (a % b)) % b
}

/// Finds the next value in the sequence: (max=26)
///     [25, 24, 25]
///     [25, 25,  0]
///     [25, 25,  1]
///     [25, 25,  2]
///         ...
///     [ 0,  0,  0,  0]
///     [ 0,  0,  0,  1]
///     [ 0,  0,  0,  2]
///         ...
///
/// # Example
///
/// ```rust
/// # use classic_crypto::util;
/// let mut arr = vec![25, 24, 25];
/// util::next(&mut arr, 26);
/// assert_eq!(&arr, &[25, 25, 0]);
///
/// let mut arr = vec![25, 25, 25];
/// util::next(&mut arr, 26);
/// assert_eq!(&arr, &[0, 0, 0, 0]);
/// ```
pub fn next<T: Integer>(arr: &mut Vec<T>, max: T) {
    // traverse from the last entry, incrementing the value.
    for elem in arr.iter_mut().rev() {
        let next = *elem + T::ONE;

        if next >= max {
            *elem = T::ZERO;
        } else {
            *elem = next;
            break;
        }
    }

    // if arr == [0,0,0,0,0,0,...,0] then the whole array needs to be incremented.
    if arr.iter().all(|&elem| elem == T::ZERO) {
        arr.push(T::ZERO);
    }
}

/// Solves a pair of equations of the form:
///     a * V + b * 1 = X
///     a * W + b * 1 = Y
/// to find the constants: a,b.
pub fn solve_linear_modular<T: Integer + Signed>(
    coefficients: [T; 2],
    equals: [T; 2],
    m: T,
) -> Option<(T, T)> {
    let [v, w] = coefficients;
    let [x, y] = equals;

    // |V  1| |a| = |X|
    // |W  1| |b|   |Y|
    let mat = Matrix::from([[v, T::ONE], [w, T::ONE]]);

    let inv = mat.mod_inv(m).unwrap();

    let ab = (inv * Vector::from([x, y])) % m;
    let [a, b] = ab.inner;

    Some((a, b))
}

/// Calculates the dot product.
pub fn dp<T: Copy + Mul<T, Output = T> + Sum<T>>(a: &[T], b: &[T]) -> T {
    a.iter().zip(b).map(|(&a, &b)| a * b).sum()
}

/// Calculates cos(theta), the angle between two vectors a and b (uses the shortest
/// common length).
pub fn scalar_product(a: &[f32], b: &[f32]) -> f32 {
    let ab = dp(a, b);
    let aa = dp(a, a);
    let bb = dp(b, b);
    ab / (aa * bb).sqrt()
}
