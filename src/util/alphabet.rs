//! The substitution [Alphabet] key type.

use crate::util;

/// A list of integers that acts as a 1:1 substitution.
#[derive(Debug, Clone, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Alphabet {
    pub sub: Vec<i32>,
    inv: Vec<i32>,
}

impl Alphabet {
    pub fn new(sub: Vec<i32>) -> Self {
        let inv = util::invert(&sub);
        Self { sub, inv }
    }

    pub fn forward(&self, cp: i32) -> i32 {
        self.sub[cp as usize]
    }

    pub fn backward(&self, cp: i32) -> i32 {
        self.inv[cp as usize]
    }
}
