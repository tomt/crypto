//! Traits for working with primitive numbers.

use crate::util;
use std::{
    iter::{Product, Sum},
    ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Rem, RemAssign, Sub, SubAssign},
};

/// Implemented by types with an identity value.
pub trait Identity {
    /// Returns the identity value, 1 for numbers, or
    /// 0 for some ciphers, or the identity matrix.
    fn identity() -> Self;
}

impl<T: Num> Identity for T {
    fn identity() -> Self {
        Self::ONE
    }
}

/// The modular inverse operation: applies to integers
/// and integer matrices.
pub trait ModularInverse: Sized {
    type Modulo;

    /// Computes the modular inverse of `self`, mod `m`.
    fn mod_inv(self, m: Self::Modulo) -> Option<Self>;
}

impl<T: Integer> ModularInverse for T {
    type Modulo = Self;

    fn mod_inv(self, m: Self::Modulo) -> Option<Self> {
        util::mmi(self, m)
    }
}

/// Represents the number 1 in the type.
pub trait One {
    /// Constant representing 1.
    const ONE: Self;
}

/// Represents the number 0 in the type.
pub trait Zero {
    /// Constant representing 0.
    const ZERO: Self;
}

/// Trait that represents primitive numbers.
pub trait Num:
    One
    + Zero
    + Copy
    + Mul<Output = Self>
    + MulAssign
    + Div<Output = Self>
    + DivAssign
    + Add<Output = Self>
    + AddAssign
    + Sub<Output = Self>
    + SubAssign
    + Sum
    + Product
    + PartialEq<Self>
    + PartialOrd<Self>
{
}

/// For working with permutations. The inverse can be computed
/// efficiently by taking a vec of
///     v[i] = x;
/// and mapping to
///     v_prime[x] = v[i];
/// but only `usize` is allowed to do this kind of indexing, so
/// this trait defines conversion methods for various types into
/// and from `usize`.
pub trait NumIndex: Integer {
    /// Trivial conversion from `Self` to `usize`.
    fn as_usize(self) -> usize;
    /// Trivial conversion from `usize` to `Self`.
    fn from_usize(n: usize) -> Self;
}

/// Trait representing integer types.
pub trait Integer: Num + Eq + Rem<Output = Self> + RemAssign + Ord + Eq {}

/// Trait representing floating point numbers.
pub trait Float: Num {
    fn sqrt(self) -> Self;
    fn powi(self, pow: i32) -> Self;
}

impl Float for f32 {
    fn sqrt(self) -> Self {
        self.sqrt()
    }
    fn powi(self, pow: i32) -> Self {
        self.powi(pow)
    }
}
impl Float for f64 {
    fn sqrt(self) -> Self {
        self.sqrt()
    }
    fn powi(self, pow: i32) -> Self {
        self.powi(pow)
    }
}

/// Trait representing signed numbers.
pub trait Signed: Num + Neg<Output = Self> {
    const MINUS_ONE: Self;
}

macro_rules! impl_signed {
    ($ty:ty,$minus_one:expr) => {
        impl Signed for $ty {
            const MINUS_ONE: Self = $minus_one;
        }
    };
}

impl_signed!(isize, -1);
impl_signed!(i64, -1);
impl_signed!(i32, -1);
impl_signed!(i8, -1);
impl_signed!(f64, -1.0);
impl_signed!(f32, -1.0);

macro_rules! impl_num_traits {
    ($ty:ty,$zero:expr,$one:expr) => {
        impl Zero for $ty {
            const ZERO: Self = $zero;
        }
        impl One for $ty {
            const ONE: Self = $one;
        }
        impl Num for $ty {}
    };
}

impl_num_traits!(u8, 0, 1);
impl_num_traits!(i8, 0, 1);
impl_num_traits!(u16, 0, 1);
impl_num_traits!(i16, 0, 1);
impl_num_traits!(u32, 0, 1);
impl_num_traits!(i32, 0, 1);
impl_num_traits!(u64, 0, 1);
impl_num_traits!(i64, 0, 1);
impl_num_traits!(f32, 0.0, 1.0);
impl_num_traits!(f64, 0.0, 1.0);
impl_num_traits!(usize, 0, 1);
impl_num_traits!(isize, 0, 1);

macro_rules! impl_integer {
    ($ty:ty,) => {
        impl Integer for $ty {}
    };
    ($ty:ty, $($rest:ty,)*) => {
        impl_integer!($ty,);
        impl_integer!($($rest,)*);
    };
}

impl_integer!(u8, i8, u16, i16, u32, i32, u64, i64, usize, isize,);

macro_rules! impl_num_index {
    ($for:ty) => {
        impl NumIndex for $for {
            #[inline(always)]
            fn as_usize(self) -> usize {
                self as usize
            }
            #[inline(always)]
            fn from_usize(n: usize) -> Self {
                n as $for
            }
        }
    };
}

impl_num_index!(isize);
impl_num_index!(usize);
impl_num_index!(i64);
impl_num_index!(u64);
impl_num_index!(u32);
impl_num_index!(i32);
impl_num_index!(u8);
impl_num_index!(i8);
