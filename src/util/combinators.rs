//! Compose functions to implement encryption and decryption.

use std::ops::{Add, Index, IndexMut, Sub};

use crate::prelude::*;
use crate::util::{divide, Keyword};

#[derive(Debug)]
pub enum Err {
    SubSliceExact,
    IsPadded(PadTo),
    Key(Box<dyn std::error::Error>),
}

pub type Result<T> = std::result::Result<T, Err>;

#[derive(Clone, Copy, Debug)]
pub enum TransposeMode {
    ReadRowWriteRow,
    ReadRowWriteCol,
    ReadColWriteRow,
    ReadColWriteCol,
}

/// Performs a grid-based transposition of the input.
/// Must be padded in advance (otherwise performs myszkowski-style
/// transposition), and any punctuation characters must also be
/// removed prior to applying this function.
pub fn transpose<T>(_mode: TransposeMode, _keyword: &Keyword) -> impl FnMut(Vec<T>) -> Vec<T> {
    move |_v| todo!()
}

pub fn modulo(b: i32) -> impl FnMut(Vec<i32>) -> Vec<i32> {
    sub(move |cp| util::modulo(cp, b))
}

/// A streaming cipher: takes a stream of type
///     S: IntoIterator<Item = Q>
/// where Q is the element type of the iterator S
/// and a function
///     F: FnMut(T, Q) -> U
/// where T is the element type of the input, Q is the
/// element type of the stream, and U is the element
/// type of the output.
pub fn stream<T, U, F, S, Q>(stream: S, mut combine: F) -> impl FnOnce(Vec<T>) -> Vec<U>
where
    S: IntoIterator<Item = Q>,
    F: FnMut(T, Q) -> U,
{
    move |v| {
        v.into_iter()
            .zip(stream)
            .map(|(c, q)| combine(c, q))
            .collect()
    }
}

/// Adds a stream to a Vec.
pub fn add<T, U, S, Q>(stream: S) -> impl FnOnce(Vec<T>) -> Vec<U>
where
    S: IntoIterator<Item = Q>,
    T: Add<Q, Output = U>,
{
    self::stream(stream, |t, q| t + q)
}

/// Subtracts a stream from a Vec.
pub fn subtract<T, U, S, Q>(stream: S) -> impl FnOnce(Vec<T>) -> Vec<U>
where
    S: IntoIterator<Item = Q>,
    T: Sub<Q, Output = U>,
{
    self::stream(stream, |t, q| t - q)
}

/// Reverses the input.
pub fn reverse(mut v: Vec<i32>) -> Vec<i32> {
    v.reverse();
    v
}

/// Applies a substitution function to each element of the vec.
pub fn sub<F>(mut f: F) -> impl FnMut(Vec<i32>) -> Vec<i32>
where
    F: FnMut(i32) -> i32,
{
    move |mut v| {
        v.iter_mut().for_each(|ch| *ch = f(*ch));
        v
    }
}

/// Apply a function to each slice of codepoints of length `group`.
pub fn sub_slice<F>(mut f: F, group: usize) -> impl FnMut(Vec<i32>) -> Vec<i32>
where
    F: FnMut(&mut [i32]),
{
    move |mut v| {
        v.chunks_mut(group).for_each(&mut f);
        v
    }
}

/// Applies a substitution function to a fixed-length array. Will return
/// `Err(SubSliceExact)` if the input is not a multiple of `N`.
pub fn sub_slice_exact<const N: usize, F>(mut f: F) -> impl FnMut(Vec<i32>) -> Result<Vec<i32>>
where
    F: FnMut([i32; N]) -> [i32; N],
{
    move |mut v| {
        if v.len() % N != 0 {
            Err(Err::SubSliceExact)
        } else {
            let mut buf = [0; N];

            for i in (0..v.len()).step_by(N) {
                buf.copy_from_slice(&v[i..][..N]);
                f(buf);
                v[i..][..N].copy_from_slice(&buf);
            }

            Ok(v)
        }
    }
}

/// Represents the index of an element in an input `Vec`.
#[derive(Clone, Copy, Debug)]
pub struct Idx(pub usize);

/// Criteria for padding schemes.
#[derive(Clone, Copy, Debug)]
pub enum PadTo {
    AtLeast(usize),
    Multiple(Period),
}

/// Verifies that the input text length matches the padding criteria.
pub fn is_padded<T>(to: PadTo, v: &[T]) -> bool {
    use PadTo::*;
    match to {
        AtLeast(k) => v.len() >= k,
        Multiple(p) => p.is_multiple(v),
    }
}

pub fn pad(to: PadTo, with: i32) -> impl FnMut(Vec<i32>) -> Vec<i32> {
    pad_with(to, move |_| with)
}

/// Adds the result of the function call with zero-based indices to the end
/// of the input until it satisfies the padding criteria.
pub fn pad_with<F>(to: PadTo, mut f: F) -> impl FnMut(Vec<i32>) -> Vec<i32>
where
    F: FnMut(Idx) -> i32,
{
    move |mut v| {
        let len = v.len();
        let extra = match to {
            PadTo::AtLeast(k) if len < k => k - len,
            PadTo::Multiple(Period(n)) if v.len() % n > 0 => n - (v.len() % n),
            _ => 0,
        };

        v.extend((0..extra).map(|i| f(Idx(i))));

        v
    }
}

/// Represents the period length of a periodic cipher.
#[derive(Clone, Copy, Debug)]
pub struct Period(pub usize);

impl Period {
    pub fn is_multiple<T>(&self, v: &[T]) -> bool {
        v.len() % self.0 == 0
    }

    /// Checks whether the period is likely to be correct.
    pub fn is_likely(&self, ciphertext: &Msg) -> bool {
        /// Threshold for periodic index of coincidence.
        const IOC_THRESHOLD: f32 = 0.045;
        /// Minimum ciphertext length to consider IOC.
        const IOC_LENGTH_THRESHOLD: usize = 150;

        // compute ioc for each period.
        ciphertext.len() <= IOC_LENGTH_THRESHOLD
            || stats::periodic_ioc(ciphertext, *self) >= IOC_THRESHOLD
    }

    pub fn column(&self, i: usize) -> PeriodIdx {
        PeriodIdx(i % self.0)
    }

    pub fn columns(&self) -> impl Iterator<Item = PeriodIdx> {
        (0..self.0).map(PeriodIdx)
    }
}

/// Represents the period index in a periodic cipher.
#[derive(Clone, Copy, Debug)]
pub struct PeriodIdx(pub usize);

impl<T> Index<PeriodIdx> for Vec<T> {
    type Output = T;

    fn index(&self, PeriodIdx(i): PeriodIdx) -> &Self::Output {
        self.index(i)
    }
}

impl<T> IndexMut<PeriodIdx> for Vec<T> {
    fn index_mut(&mut self, PeriodIdx(i): PeriodIdx) -> &mut Self::Output {
        self.index_mut(i)
    }
}

/// A periodic cipher in which each period is encrypted
/// according to a function of the kind:
///     F: FnMut(Period) -> G
///     G: FnMut(Vec<i32>) -> Vec<i32>
/// ie. F is a selector function of the current period,
/// and G has the type of any combinator function (like
/// those in this module, or that of a general cipher).
///
/// Requies allocations.
pub fn periodic_with<F, G>(period: Period, mut f: F) -> impl FnMut(Vec<i32>) -> Vec<i32>
where
    F: FnMut(PeriodIdx) -> G,
    G: FnMut(Vec<i32>) -> Vec<i32>,
{
    let Period(period) = period;

    move |v| {
        divide(period, v)
            .into_iter()
            .zip((0..period).map(PeriodIdx))
            .flat_map(|(cp, period)| f(period)(cp))
            .collect()
    }
}

/// A periodic cipher: takes a function choose:
///     C: FnMut(Period) -> F
///     F: FnMut(i32) -> i32
pub fn periodic<C, F>(period: Period, choose: C) -> impl FnMut(Vec<i32>) -> Vec<i32>
where
    C: FnMut(PeriodIdx) -> F + Clone,
    F: FnMut(i32) -> i32,
{
    let choose = move |period| {
        let mut choose = choose.clone();
        move |cp, _, _| choose(period)(cp)
    };

    periodic_with_last(period, choose)
}

/// A periodic cipher: takes a function choose:
///     C: FnMut(Period) -> F
///     F: FnMut(i32) -> i32
/// Iterator version.
pub fn periodic_iter<I, C, F>(period: Period, mut choose: C, iter: I) -> impl Iterator<Item = i32>
where
    I: Iterator<Item = i32>,
    C: FnMut(PeriodIdx) -> F + Clone,
    F: FnMut(i32) -> i32,
{
    iter.enumerate()
        .map(move |(idx, ch)| (PeriodIdx(idx % period.0), ch))
        .map(move |(pidx, ch)| choose(pidx)(ch))
}

/// A periodic cipher in which each element is computed
/// from the result of the previous computation (if
/// any), and the current index. Takes
///     C: FnMut(Period) -> F
///     F: FnMut(i32, Index, Option<i32>) -> i32
pub fn periodic_with_last<C, F>(period: Period, mut choose: C) -> impl FnMut(Vec<i32>) -> Vec<i32>
where
    C: FnMut(PeriodIdx) -> F,
    F: FnMut(i32, Idx, Option<i32>) -> i32,
{
    move |mut v| {
        for index in 0..v.len() {
            let last = index.gt(&period.0).then(|| v[index - period.0]);
            v[index] = choose(PeriodIdx(index % period.0))(v[index], Idx(index), last);
        }

        v
    }
}
