//! Generating primes to factor integers.

/// Can be used to factor integers or iterate over primes.
pub struct Primes {
    // The list of primes
    primes: Vec<usize>,
}

impl Default for Primes {
    fn default() -> Self {
        Primes { primes: vec![2] }
    }
}

impl Primes {
    /// An iterator over the prime numbers.
    pub fn iter(&mut self) -> PrimesIter<'_> {
        PrimesIter {
            index: 0,
            inner: self,
        }
    }

    /// Factor `n`.
    pub fn factor(&mut self, mut n: usize) -> Vec<usize> {
        if n < 2 {
            vec![n]
        } else {
            let mut primes = self.iter();
            let mut factors = vec![];

            while n != 1 {
                if let Some(prime) = primes.next() {
                    while n % prime == 0 {
                        n /= prime;
                        factors.push(prime);
                    }
                }
            }

            factors
        }
    }
}

/// An iterator over the prime numbers.
pub struct PrimesIter<'p> {
    index: usize,
    inner: &'p mut Primes,
}

impl<'p> Iterator for PrimesIter<'p> {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        // continue generating primes until the index is reached.
        'outer: while self.index + 1 >= self.inner.primes.len() {
            let &last_prime = self.inner.primes.last().unwrap();
            'search: for n in 1 + last_prime.. {
                let sqrt = (n as f32).sqrt().ceil() as usize;

                for p in self.inner.primes.iter().take_while(|&&p| p <= sqrt) {
                    if n % p == 0 {
                        continue 'search;
                    }
                }

                self.inner.primes.push(n);
                continue 'outer;
            }
        }

        // index has been reached, return the prime.
        self.index += 1;
        self.inner.primes.get(self.index - 1).copied()
    }
}

#[cfg(test)]
mod tests {
    use super::Primes;

    #[test]
    fn factor() {
        let mut primes = Primes::default();

        assert_eq!(primes.factor(1234), vec![2, 617]);
        assert_eq!(primes.factor(100), vec![2, 2, 5, 5]);
        assert_eq!(primes.factor(1), vec![1]);
        assert_eq!(primes.factor(0), vec![0]);
    }

    #[test]
    fn prime_gen_test() {
        let primes: Vec<usize> = Primes::default().iter().take(10).collect();
        assert_eq!(primes, &[2, 3, 5, 7, 11, 13, 17, 19, 23, 29]);
    }
}
