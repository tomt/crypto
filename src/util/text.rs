//! Utilities for string manipulation.

use std::collections::HashSet;

/// Returns true if the string contains any repeated characters.
///
/// # Example
///
/// ```
/// # use classic_crypto::util::text;
/// assert!(text::has_repeats("abca"));
/// assert!(text::has_repeats("AbcA"));
/// assert!(!text::has_repeats("Abca"));
/// assert!(!text::has_repeats("abcdefghijklmn"));
/// assert!(!text::has_repeats(""));
/// ```
pub fn has_repeats(msg: &str) -> bool {
    let mut set: HashSet<char> = HashSet::default();

    for ch in msg.chars() {
        if !set.insert(ch) {
            return true;
        }
    }

    false
}

/// Remove all whitespace then seperate each block of `block_size` chars
/// by `sep`.
///
/// # Example
///
/// ```
/// # use classic_crypto::util::text;
/// assert_eq!(&text::join_blocks("AAABBBCCC", 3, " "), "AAA BBB CCC");
/// assert_eq!(&text::join_blocks("AAAABBBBC", 4, "-"), "AAAA-BBBB-C");
/// ```
pub fn join_blocks(msg: &str, n: usize, sep: &str) -> String {
    join(blocks(&delete_whitespace(msg), n), sep)
}

/// Joins the elements of the iterator with `sep`.
pub fn join<'a>(iter: impl Iterator<Item = &'a str>, sep: &str) -> String {
    let mut buf = String::new();
    let mut iter = iter.peekable();

    while let Some(s) = iter.next() {
        buf.push_str(s);

        if iter.peek().is_some() {
            buf.push_str(sep);
        }
    }

    buf
}

/// Removes whitesapce from a string.
///
/// # Examples
///
/// ```
/// # use classic_crypto::util::text;
/// assert_eq!(&text::delete_whitespace("a   b\t\n  cDEF."), "abcDEF.");
/// ````
pub fn delete_whitespace(msg: &str) -> String {
    msg.chars().filter(|c| !c.is_whitespace()).collect()
}

/// Finds the number of differing `char`s between `a` and `b`.
///
/// # Example
///
/// ```
/// # use classic_crypto::util::text;
/// let a = "abcdefg";
/// let b = "acbdeeg";
/// assert_eq!(text::diff_count(a, b), 3);
/// ```
pub fn diff_count(a: &str, b: &str) -> usize {
    a.chars().zip(b.chars()).filter(|(a, b)| a != b).count()
}

/// Divides a string into blocks of length `n`.
pub struct Blocks<'a> {
    n: usize,
    slice: &'a str,
}

impl<'a> Iterator for Blocks<'a> {
    type Item = &'a str;

    fn next(&mut self) -> Option<Self::Item> {
        if self.slice.is_empty() || self.n == 0 {
            return None;
        }

        let boundary = self
            .slice
            .char_indices()
            .map(|(idx, _)| idx)
            .nth(self.n)
            .unwrap_or(self.slice.len());

        let (slice, remaining) = self.slice.split_at(boundary);

        self.slice = remaining;
        Some(slice)
    }
}

/// Iterates over slices with (at most) `n` `chars` in them.
pub fn blocks(msg: &str, n: usize) -> Blocks<'_> {
    Blocks { n, slice: msg }
}
