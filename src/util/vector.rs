//! Definition of a [Vector] type and vector operations.

use crate::util::{
    self,
    num::{Float, Integer, Num},
};
use std::{
    fmt::Display,
    ops::{Mul, Rem},
    slice,
};

/// A generic vector type.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Vector<const N: usize, T> {
    /// The components.
    pub inner: [T; N],
}

impl<const N: usize, T: Display> Display for Vector<N, T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[")?;

        for i in 0..self.inner.len() {
            write!(f, "{}", self.inner[i])?;
            if i + 1 < self.inner.len() {
                write!(f, ", ")?;
            }
        }

        write!(f, "]")
    }
}

impl<const N: usize, T: Num> Vector<N, T> {
    /// Computes the dot product.
    pub fn dot(&self, other: Vector<N, T>) -> T {
        self.into_iter().zip(&other).map(|(&a, &b)| a * b).sum()
    }
}

impl<const N: usize, T: Float> Vector<N, T> {
    /// Computes cos(theta) where theta is the angle between
    /// the two vectors.
    pub fn scalar(&self, other: Vector<N, T>) -> T {
        let ab = self.dot(other);
        let aa = self.dot(*self);
        let bb = other.dot(other);

        ab / (aa * bb).sqrt()
    }
}

impl<T: Num> Vector<3, T> {
    /// Finds the cross product of this vector with another.
    pub fn cross(&self, other: Self) -> Self {
        let [a, b, c] = self.inner;
        let [d, e, f] = other.inner;

        Self {
            inner: [b * f - c * e, c * d - a * f, a * e - b * d],
        }
    }
}

impl<const N: usize, T: Float> Vector<N, T> {
    /// Finds the magnitude of the vector. Restricted to floating-point
    /// types.
    pub fn magnitude(&self) -> T {
        self.dot(*self).sqrt()
    }
}

impl<const N: usize, T> From<[T; N]> for Vector<N, T> {
    fn from(inner: [T; N]) -> Self {
        Self { inner }
    }
}

impl<'a, const N: usize, T> IntoIterator for &'a Vector<N, T> {
    type Item = &'a T;
    type IntoIter = slice::Iter<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        self.inner.iter()
    }
}

impl<const N: usize, T: Num> Mul<T> for Vector<N, T> {
    type Output = Self;

    fn mul(self, rhs: T) -> Self::Output {
        Self {
            inner: self.inner.map(|x| x * rhs),
        }
    }
}

impl<const N: usize, T: Integer> Rem<T> for Vector<N, T> {
    type Output = Self;

    fn rem(self, rhs: T) -> Self::Output {
        Self {
            inner: self.inner.map(|elem| util::modulo(elem, rhs)),
        }
    }
}
