/// Definition of a [Matrix] type and matrix/vector operations,
/// including modular inverses.
use crate::util::{
    self, even,
    num::{Identity, Integer, ModularInverse, Num, One, Zero},
    vector::Vector,
};
use std::{
    array,
    fmt::Display,
    ops::{Index, IndexMut, Mul, MulAssign, Neg, Rem, RemAssign},
};

use super::Signed;

/// Finds the determinant of an `n` * `n` matrix, `mat`.
pub fn det<T: Num + Neg<Output = T>>(n: usize, mat: &[T]) -> T {
    match mat {
        [t] => *t,
        mat => (0..n)
            .map(|col| match even(col) {
                true => mat[col] * det(n - 1, &reduce(0, col, n, mat)),
                false => -mat[col] * det(n - 1, &reduce(0, col, n, mat)),
            })
            .sum(),
    }
}

/// Reduces a matrix of size `n` * `n` to an `n-1` * `n-1`
/// matrix by excluding `row` and `col`.
pub fn reduce<T: Copy>(row: usize, col: usize, n: usize, mat: &[T]) -> Vec<T> {
    (0..n * n)
        .filter(|&i| (i / n != row && i % n != col))
        .map(|i| mat[i])
        .collect()
}

/// A square (NxN) matrix type.
pub type SquareMatrix<const N: usize, T> = Matrix<N, N, T>;

/// A generic matrix type.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Matrix<const M: usize, const N: usize, T> {
    pub inner: [[T; N]; M],
}

impl<const N: usize, T: Integer + Signed> ModularInverse for Matrix<N, N, T> {
    type Modulo = T;

    fn mod_inv(self, m: Self::Modulo) -> Option<Self> {
        let det = util::modulo(self.det(), m);
        let mmi = util::mmi(det, m)?;
        let cofactors = self.cofactors().transpose();
        Some((cofactors * mmi) % m)
    }
}

impl<const N: usize, const M: usize, T: Copy> Matrix<M, N, T> {
    /// Gets a row of the matrix as a vector.
    pub fn row(&self, row: usize) -> Vector<N, T> {
        Vector::from(self.inner[row])
    }

    /// Gets a column of the matrix as a vector.
    pub fn col(&self, col: usize) -> Vector<M, T> {
        Vector::from(array::from_fn(|row| self[(row, col)]))
    }

    /// Computes the transpose of the matrix.
    pub fn transpose(&self) -> Matrix<N, M, T> {
        Matrix {
            inner: array::from_fn(|col| array::from_fn(|row| self[(row, col)])),
        }
    }
}

impl<const N: usize, T: Zero + One> Identity for Matrix<N, N, T> {
    fn identity() -> Self {
        Matrix::from(array::from_fn(|row| {
            array::from_fn(|col| if col == row { T::ONE } else { T::ZERO })
        }))
    }
}

impl<const N: usize, T: Num + Neg<Output = T>> Matrix<N, N, T> {
    /// Recursively computes the determinant of a square matrix.
    pub fn det(&self) -> T {
        det(N, &Vec::from(*self))
    }

    /// Generates the matrix of cofactors.
    pub fn cofactors(&self) -> Self {
        let mat = Vec::from(*self);
        let multiplier = |row, col| if row % 2 == col % 2 { T::ONE } else { -T::ONE };

        Self {
            inner: array::from_fn(|row| {
                array::from_fn(|col| multiplier(row, col) * det(N - 1, &reduce(row, col, N, &mat)))
            }),
        }
    }
}

impl<const N: usize, const M: usize, T: Copy> From<Matrix<M, N, T>> for Vec<T> {
    fn from(matrix: Matrix<M, N, T>) -> Self {
        (0..N * N).map(|i| matrix[(i / N, i % N)]).collect()
    }
}

impl<const M: usize, const N: usize, T> From<[[T; N]; M]> for Matrix<M, N, T> {
    fn from(inner: [[T; N]; M]) -> Self {
        Self { inner }
    }
}

impl<const M: usize, const N: usize, T: Zero + Copy> Zero for Matrix<M, N, T> {
    const ZERO: Self = Self {
        inner: [[T::ZERO; N]; M],
    };
}

impl<const M: usize, const N: usize, T> Index<usize> for Matrix<M, N, T> {
    type Output = [T; N];

    fn index(&self, index: usize) -> &Self::Output {
        self.inner.index(index)
    }
}
impl<const M: usize, const N: usize, T> Index<(usize, usize)> for Matrix<M, N, T> {
    type Output = T;

    fn index(&self, (row, column): (usize, usize)) -> &Self::Output {
        self.inner.index(row).index(column)
    }
}
impl<const M: usize, const N: usize, T> IndexMut<(usize, usize)> for Matrix<M, N, T> {
    fn index_mut(&mut self, (row, column): (usize, usize)) -> &mut Self::Output {
        self.inner.index_mut(row).index_mut(column)
    }
}

impl<const M: usize, const N: usize, T: Display> Display for Matrix<M, N, T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[")?;

        for row in 0..M {
            if row > 0 {
                write!(f, " ")?;
            }

            write!(f, "[")?;

            for column in 0..N {
                write!(f, "{}", self[(row, column)])?;

                if column + 1 < N {
                    write!(f, ", ")?;
                }
            }

            if row + 1 < M {
                writeln!(f, "],")?;
            }
        }

        writeln!(f, "]]")
    }
}

impl<const M: usize, const N: usize, const P: usize, T: Num> Mul<Matrix<P, N, T>>
    for Matrix<M, P, T>
{
    type Output = Matrix<M, N, T>;

    fn mul(self, rhs: Matrix<P, N, T>) -> Self::Output {
        Matrix {
            inner: array::from_fn(|row| array::from_fn(|col| self.row(row).dot(rhs.col(col)))),
        }
    }
}

impl<const M: usize, const N: usize, T: Num> Mul<T> for Matrix<M, N, T> {
    type Output = Self;

    fn mul(mut self, rhs: T) -> Self::Output {
        self *= rhs;
        self
    }
}
impl<const M: usize, const N: usize, T: Num> MulAssign<T> for Matrix<M, N, T> {
    fn mul_assign(&mut self, rhs: T) {
        for row in &mut self.inner {
            for element in row {
                *element *= rhs;
            }
        }
    }
}

impl<const M: usize, const N: usize, T: Integer> Rem<T> for Matrix<M, N, T> {
    type Output = Self;

    fn rem(mut self, rhs: T) -> Self::Output {
        self %= rhs;
        self
    }
}
impl<const M: usize, const N: usize, T: Integer> RemAssign<T> for Matrix<M, N, T> {
    fn rem_assign(&mut self, rhs: T) {
        for row in &mut self.inner {
            for element in row {
                *element = util::modulo(*element, rhs);
            }
        }
    }
}

impl<const M: usize, const N: usize, T: Num> Mul<Vector<N, T>> for Matrix<M, N, T> {
    type Output = Vector<M, T>;

    fn mul(self, rhs: Vector<N, T>) -> Self::Output {
        Vector::from(array::from_fn(|i| self.row(i).dot(rhs)))
    }
}

#[cfg(test)]
mod tests {
    use crate::util::{
        matrix::Matrix,
        num::{Identity, ModularInverse},
        vector::Vector,
    };

    #[test]
    fn inv_modulo_26_of_3x3() {
        let m = Matrix::from([[1, 5], [3, 4]]);
        let m_inv = m.mod_inv(26).unwrap();

        assert_eq!((m * m_inv) % 26, Matrix::identity());
        assert_eq!((m_inv * m) % 26, Matrix::identity());

        let p = Vector::from([4, 5]);

        assert_eq!((m_inv * (m * p)) % 26, p);
        assert_eq!((m * (m_inv * p)) % 26, p);
    }

    #[test]
    fn inv_modulo_26_of_2x2() {
        let m = Matrix::from([[0, 11, 15], [7, 0, 1], [4, 19, 0]]);
        let m_inv = m.mod_inv(26).unwrap();

        assert_eq!((m * m_inv) % 26, Matrix::identity());
        assert_eq!((m_inv * m) % 26, Matrix::identity());

        let p = Vector::from([4, 5, 6]);

        assert_eq!((m_inv * (m * p)) % 26, p);
        assert_eq!((m * (m_inv * p)) % 26, p);
    }
}
