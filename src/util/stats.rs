//! Functions that compute statistics relavant
//! to cryptanalysis.

use std::iter;

use nanoserde::{DeBin, SerBin};

use crate::util::{combinators::Period, Float};

/// The maximum length of the cipher alphabet.
pub const MAX_ALPHABET_LEN: usize = 32;

/// Trait to represent types that can be used to generate
/// a `usize` for text scoring.
pub trait ScoringIndex {
    /// Convert from `Self` to `usize`.
    fn as_usize(&self) -> usize;
}

impl ScoringIndex for usize {
    fn as_usize(&self) -> usize {
        *self
    }
}
impl ScoringIndex for &usize {
    fn as_usize(&self) -> usize {
        **self
    }
}

impl ScoringIndex for i32 {
    fn as_usize(&self) -> usize {
        debug_assert!(*self >= 0);

        *self as usize
    }
}
impl ScoringIndex for &i32 {
    fn as_usize(&self) -> usize {
        (*self).as_usize()
    }
}

/// Which scoring statistic to use.
#[derive(Default, Clone, Copy, Debug, Hash, PartialEq, Eq, SerBin, DeBin)]
pub enum StatsSize {
    /// Single letters.
    Unigrams,
    /// Pairs of letters.
    Bigrams,
    /// Sets of 3 letters.
    Trigrams,
    /// Sets of 4 letters.
    #[default]
    Quadgrams,
}

impl StatsSize {
    /// Gets the mask for the score size.
    pub fn mask(&self) -> usize {
        #[allow(clippy::unusual_byte_groupings)]
        match self {
            StatsSize::Unigrams => 0b_00000_00000_00000_11111,
            StatsSize::Bigrams => 0b_00000_00000_11111_11111,
            StatsSize::Trigrams => 0b_00000_11111_11111_11111,
            StatsSize::Quadgrams => 0b_11111_11111_11111_11111,
        }
    }

    /// Gets the length associated with the score size.
    pub fn length(&self) -> usize {
        match self {
            StatsSize::Unigrams => 1,
            StatsSize::Bigrams => 2,
            StatsSize::Trigrams => 3,
            StatsSize::Quadgrams => 4,
        }
    }

    /// Iterate over `StatsSize`s.
    pub fn iter() -> impl Iterator<Item = StatsSize> {
        use StatsSize::*;

        [Unigrams, Bigrams, Trigrams, Quadgrams].into_iter()
    }
}

/// Adapts an iterator over code points to iterate over a 5n bit index,
/// where n in the score size.
#[inline]
pub fn scoring_indices<I, T>(data: I, stats_size: StatsSize) -> impl Iterator<Item = usize>
where
    I: IntoIterator<Item = T>,
    T: ScoringIndex,
{
    let mut iter = data.into_iter();
    let mask = stats_size.mask();

    // take the first (size - 1) elements to build the initial index.
    let mut idx = (&mut iter)
        .take(stats_size.length() - 1)
        .fold(0, |idx, cp| (idx << 5) | cp.as_usize());

    iter.map(move |cp| {
        idx = (idx << 5) | cp.as_usize();
        idx & mask
    })
}

/// Adapts an iterator over code points to iterate over a 5n bit index,
/// where n in the score size. Iterates over indexes in blocks of n letters
/// rather than a sliding window.
///
/// # Example
///
/// ```rust
/// # use classic_crypto::prelude::*;
/// let v = vec![1, 2, 3, 4, 5, 6, 7, 8];
/// let mut iter = stats::scoring_indices_blocks(v, Trigrams);
/// assert_eq!(iter.next(), Some((1 << 10) | (2 << 5) | 3));
/// assert_eq!(iter.next(), Some((4 << 10) | (5 << 5) | 6));
/// assert_eq!(iter.next(), None);
/// ```
#[inline]
pub fn scoring_indices_blocks<I, T>(data: I, stats_size: StatsSize) -> impl Iterator<Item = usize>
where
    I: IntoIterator<Item = T>,
    T: ScoringIndex,
{
    let length = stats_size.length();
    let mask = stats_size.mask();
    let iter = data.into_iter();
    let mut idx = 0;

    iter.enumerate().filter_map(move |(i, cp)| {
        idx = (idx << 5) | cp.as_usize();
        match (i + 1) % length {
            0 => Some(idx & mask),
            _ => None,
        }
    })
}

/// Calculates counts for each 1,2,3 or 4 letter sequence. Uses a sliding
/// / overlapping window.
///
/// # Example
///
/// ```rust
/// # use classic_crypto::prelude::*;
/// let iter = [1, 2, 3, 4, 5, 4, 2, 3, 3, 1].iter().copied();
///
/// assert!(matches!(&stats::counts(iter, StatsSize::Unigrams)[..], &[
///     0,
///     2, // 1
///     2, // 2
///     3, // 3
///     2, // 4
///     1, // 5
///     ..
/// ]));
/// ```
pub fn counts<I, T>(iter: I, stats_size: StatsSize) -> Vec<usize>
where
    I: IntoIterator<Item = T>,
    T: ScoringIndex,
{
    let mut counts = vec![0; 1 << (5 * stats_size.length())];

    for idx in scoring_indices(iter, stats_size) {
        counts[idx] += 1;
    }

    counts
}

/// Calculates the counts for each 1,2,3 or 4 letter sequence. Uses blocks /
/// non-overlapping windows.
pub fn counts_blocks<I, T>(iter: I, stats_size: StatsSize) -> Vec<usize>
where
    I: IntoIterator<Item = T>,
    T: ScoringIndex,
{
    let mut counts = vec![0; 1 << (5 * stats_size.length())];

    for idx in scoring_indices_blocks(iter, stats_size) {
        counts[idx] += 1;
    }

    counts
}

/// Calculates the index of coincedence.
///
/// This is the sum for each letter in the text
/// of
/// *   n (n-1) / (N (N-1))
///
/// where
/// *   n is the number of times that the letter occurs
/// *   N is the total number of letters.
///
/// The IOC is the probability that 2 letters chosen at random
/// from a text will be the same. For substitution ciphers and
/// plaintext, this value is usually around 0.067 (english). This
/// statistic is useful for determining the type of cipher.
///
/// For `stats_size` greater than `Unigrams`, this function considers
/// each block seperately, rather than a sliding window.
pub fn ioc<T, I>(iter: I, stats_size: StatsSize) -> f32
where
    T: ScoringIndex,
    I: IntoIterator<Item = T>,
{
    ioc_of_counts(&counts_blocks(iter, stats_size))
}

/// Calculates the index of coincedence from an array of letter counts.
pub fn ioc_of_counts(counts: &[usize]) -> f32 {
    let len = counts.iter().sum::<usize>() as f32;

    // total f * (f - 1) where f is the frequency of each letter
    let total = counts
        .iter()
        .filter(|&&x| x > 0)
        .map(|&f| f * (f - 1))
        .sum::<usize>() as f32;

    total / (len * (len - 1.0))
}

/// Returns a vec of log probabilities for each item in counts.
pub fn log_probs(counts: &[usize]) -> Vec<f32> {
    let sum = counts.iter().sum::<usize>() as f32;
    let floor = (0.01 / sum).ln();
    counts
        .iter()
        .map(|&freq| match freq {
            0 => floor,
            f => ((f as f32) / sum).ln(),
        })
        .collect()
}

/// Calculates the periodic index of coincedence.
///
/// This is the average of the index of coincedence each columns in a
/// piece of text. Each column is all letters whose indices satisfy:
/// *   K + n * P
///
/// where
/// *   K is the starting value from (0..`period_length`)
/// *   P is the `period_length`
/// *   n is the position in the column
pub fn periodic_ioc<T, I>(iter: I, period: Period) -> f32
where
    I: IntoIterator<Item = T>,
    T: ScoringIndex,
{
    periodic_unigram_counts(iter, period)
        .iter()
        .map(AsRef::as_ref)
        .map(ioc_of_counts)
        .sum::<f32>()
        / (period.0 as f32)
}

/// Counts the unigrams in each period.
pub fn periodic_unigram_counts<T, I>(iter: I, period: Period) -> Vec<[usize; 32]>
where
    I: IntoIterator<Item = T>,
    T: ScoringIndex,
{
    let mut counts = (0..period.0)
        .map(|_| [0; MAX_ALPHABET_LEN])
        .collect::<Vec<_>>();
    for (idx, cp) in iter.into_iter().enumerate() {
        counts[idx % period.0][cp.as_usize()] += 1;
    }

    counts
}

/// Calculates the list of frequencies from the counts.
pub fn frequencies(counts: &[usize]) -> Vec<f32> {
    let sum = counts.iter().sum::<usize>() as f32;
    counts.iter().map(|&c| c as f32 / sum).collect()
}

/// Calculates the entropy of the data.
pub fn entropy<T, I>(iter: I, alphabet_len: usize) -> f32
where
    I: IntoIterator<Item = T>,
    T: ScoringIndex,
{
    let counts = counts(iter, StatsSize::Unigrams);
    let len = counts.iter().sum::<usize>() as f32;

    counts
        .into_iter()
        .filter(|&i| i != 0)
        .map(|i| i as f32 / len)
        .map(|i| i * -i.log(alphabet_len as f32))
        .sum()
}

/// Computes the signature of a text, by averaging the sorted unigram frequencies
/// for each period column.
pub fn signature<T, I>(iter: I, period: Period, alphabet_len: usize) -> Vec<f32>
where
    I: IntoIterator<Item = T>,
    T: ScoringIndex,
{
    periodic_unigram_counts(iter, period)
        .into_iter()
        .map(|counts| frequencies(&counts[0..alphabet_len]))
        .flat_map(|mut f| {
            f.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap());
            f
        })
        .enumerate()
        .fold(vec![0.0; alphabet_len], |mut acc, (idx, f)| {
            acc[idx % alphabet_len] += f / period.0 as f32;
            acc
        })
}

/// Finds the twist between two signatures.
pub fn twist<T: Float>(sig_a: &[T], sig_b: &[T]) -> T {
    let len = sig_a.len().min(sig_b.len());

    sig_a
        .iter()
        .zip(sig_b)
        .enumerate()
        .fold(T::ZERO, |acc, (idx, (&a, &b))| match idx < len / 2 {
            true => acc + a - b,
            false => acc + b - a,
        })
}

/// Computes the chi-squared statistic between two lists of frequencies.
pub fn chi_squared<T, I, J>(a: I, expected: J) -> T
where
    I: IntoIterator<Item = T>,
    J: IntoIterator<Item = T>,
    T: Float,
{
    iter::zip(a, expected)
        .map(|(a, e)| (a - e).powi(2) / e)
        .sum()
}

/// Computes the dot product of `Iterator::zip` of the two iterators.
pub fn dp<T, I, J>(a: I, b: J) -> T
where
    I: IntoIterator<Item = T>,
    J: IntoIterator<Item = T>,
    T: Float,
{
    iter::zip(a, b).map(|(a, b)| a * b).sum()
}
