//! See the documentation for [`Railfence`].

use crate::{
    analysis::{
        brute_force::KeySpace,
        transposition::{self, Transposition},
    },
    cipher::Cipher,
    key::{Integer, Key, KeyFrom, KeyResult, Successor},
    lang::{with_alphabet::WithAlphabet, AlphabetLen, Lang},
    util,
};

type Rails = Integer<1, 99>;

/// Railfence is a transposition cipher where the plaintext letters
/// are written in a grid in incrementing and decrementing rows,
/// according to the number of rows (rails). One written in a grid,
/// the ciphertext is read off each row and combined. To decrypt, a
/// similar grid is constructed, this time entering one row at a time.
/// The plaintext is then read in the same way that it was written.
/// The key for the cipher is the number of rails, which can be brute
/// forced to find a solution, with values from 1 to 100 tried. This
/// cipher is independent of alphabet length, but will not transpose
/// any punctuation.
#[derive(Debug)]
pub struct Railfence<'l> {
    alph: WithAlphabet<'l>,
    rails: Rails,
}

impl<'l> Railfence<'l> {
    /// Creates a new Railfence cipher, with `rails` as the number of rails
    /// (rows) in the grid. `rails` can be between 1 and 100.
    pub fn new<R>(lang: &'l Lang, rails: R) -> KeyResult<Self>
    where
        Rails: KeyFrom<'l, R>,
    {
        Ok(Self {
            alph: Self::alph(lang),
            rails: Rails::key_from(Self::alph(lang), rails)?,
        })
    }
    /// Gets the number of rails.
    pub fn rails(&self) -> &Rails {
        &self.rails
    }
}
impl<'l> Cipher<'l> for Railfence<'l> {
    fn alphabet_len() -> AlphabetLen {
        AlphabetLen::Any
    }
    fn random(lang: &'l Lang) -> Self {
        Self {
            alph: Self::alph(lang),
            rails: Rails::random(Self::alph(lang)),
        }
    }
    fn default(lang: &'l Lang) -> Self {
        Self {
            alph: Self::alph(lang),
            rails: Rails::default(Self::alph(lang)),
        }
    }
    fn encrypt(&self, msg: &str) -> String {
        transposition::encrypt(msg, self.alph, self)
    }
    fn decrypt(&self, msg: &str) -> String {
        transposition::decrypt(msg, self.alph, self)
    }
}
impl<'l> Transposition for Railfence<'l> {
    fn encrypt_indices(&self, len: usize, buf: &mut Vec<usize>) {
        buf.clear();

        let rails = self.rails.value();

        (0..rails)
            .flat_map(|row| {
                (0..rails)
                    .chain((1..rails - 1).rev())
                    .cycle()
                    .enumerate()
                    .filter_map(move |(idx, r)| if r == row { Some(idx) } else { None })
                    .take_while(|&idx| idx < len)
            })
            .for_each(|idx| buf.push(idx));
    }
    fn decrypt_indices(&self, len: usize, buf: &mut Vec<usize>) {
        let mut key = Vec::with_capacity(len);
        self.encrypt_indices(len, &mut key);
        util::invert_into(&key, buf);
    }
}
impl<'l> KeySpace<'l> for Railfence<'l> {
    type Iter = RailfenceKeySpace<'l>;
    type Limit = ();

    fn key_space(lang: &'l Lang, _: ()) -> Self::Iter {
        RailfenceKeySpace {
            curr: Some(Railfence::default(lang)),
        }
    }
}

/// An iterator over the key space of the Railfence cipher.
pub struct RailfenceKeySpace<'l> {
    curr: Option<Railfence<'l>>,
}

impl<'l> Iterator for RailfenceKeySpace<'l> {
    type Item = Railfence<'l>;

    fn next(&mut self) -> Option<Self::Item> {
        let curr = self.curr.take()?;
        self.curr = curr.rails.successor(curr.alph).map(|rails| Railfence {
            alph: curr.alph,
            rails,
        });

        Some(curr)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{common::LANG, key::KeyError, prelude::BruteForce};

    #[test]
    fn new() {
        assert!(matches!(
            Railfence::new(&LANG, 0),
            Err(KeyError::OutOfRange {
                num: 0,
                min: 1,
                max: 100,
            })
        ));
        assert!(matches!(
            Railfence::new(&LANG, 101),
            Err(KeyError::OutOfRange {
                num: 101,
                min: 1,
                max: 100,
            })
        ));
        assert!(Railfence::new(&LANG, 1).is_ok());
        assert!(Railfence::new(&LANG, 100).is_ok());
    }

    #[test]
    fn encrypt_and_decrypt() {
        let cipher = Railfence::new(&LANG, 4).unwrap();

        let plaintext =
            "Why, sometimes I've believed as many as six impossible things before breakfast.";
        let ciphertext = &cipher.encrypt(plaintext);

        assert_eq!(
            ciphertext,
            "Wei, issplgrkh M'ts vleamasm ob enso ea fyo ieeevdayii sitibf beatsm benxshers."
        );
        assert_eq!(&cipher.decrypt(ciphertext), plaintext);
    }

    #[test]
    fn solve() {
        let ciphertext =
            "Wei, issplgrkh M'ts vleamasm ob enso ea fyo ieeevdayii sitibf beatsm benxshers.";
        let Railfence { rails, .. } = Railfence::brute_force(&LANG, ciphertext, Default::default());

        assert_eq!(rails.value(), 4);
    }
}
