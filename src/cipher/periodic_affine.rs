//! See the documentation for [`PeriodicAffine`].

use crate::{
    analysis::{brute_force::KeySpace, Solve},
    cipher::Cipher,
    key::{CodePoint, Coprime, Key, KeyError, KeyFrom, KeyResult},
    lang::{
        with_alphabet::{StatsSize, WithAlphabet},
        AlphabetLen, Lang,
    },
    prelude::LettersExt,
    util,
};

use super::Affine;

/// The [`Options`](Solve::Options) type for [`PeriodicAffine`]. Used to
/// configure [`Solve::solve`].
pub struct PeriodicAffineSolve {
    /// The [`StatsSize`] that should be used for scoring.
    pub stats_size: StatsSize,
    /// The maximum period that should be tested.
    pub max_period: usize,
}
impl Default for PeriodicAffineSolve {
    fn default() -> Self {
        Self {
            stats_size: StatsSize::default(),
            max_period: 30,
        }
    }
}

/// A periodic cipher where letters in each period are encrypted by
/// an individual affine cipher.
#[derive(Debug)]
pub struct PeriodicAffine<'l> {
    alph: WithAlphabet<'l>,
    key: Vec<(Coprime, CodePoint)>,
}

impl<'l> PeriodicAffine<'l> {
    /// Creates a new [`PeriodicAffine`] cipher with the `keyword`
    /// provided.
    pub fn new<I, T, U>(lang: &'l Lang, key: I) -> KeyResult<Self>
    where
        I: IntoIterator<Item = (T, U)>,
        Coprime: KeyFrom<'l, T>,
        CodePoint: KeyFrom<'l, U>,
    {
        let mut k = vec![];

        for (t, u) in key {
            k.push((
                Coprime::key_from(Self::alph(lang), t)?,
                CodePoint::key_from(Self::alph(lang), u)?,
            ));
        }

        if k.is_empty() {
            return Err(KeyError::EmptyKeyword);
        }

        Ok(Self {
            alph: Self::alph(lang),
            key: k,
        })
    }
    /// Gets a slice of the (a, b) tuples for each period.
    pub fn key(&self) -> &[(Coprime, CodePoint)] {
        &self.key
    }
    /// Gets the period of the cipher.
    pub fn period(&self) -> usize {
        self.key.len()
    }
}
impl<'l> Cipher<'l> for PeriodicAffine<'l> {
    fn alphabet_len() -> AlphabetLen {
        AlphabetLen::Any
    }
    fn random(lang: &'l Lang) -> Self {
        let alph = Self::alph(lang);

        Self {
            alph,
            key: (0..fastrand::usize(3..12))
                .map(|_| (Coprime::random(alph), CodePoint::random(alph)))
                .collect(),
        }
    }
    fn default(lang: &'l Lang) -> Self {
        let alph = Self::alph(lang);

        Self {
            alph,
            key: vec![(Coprime::default(alph), CodePoint::default(alph))],
        }
    }
    fn encrypt(&self, msg: &str) -> String {
        let mut idx = 0;
        self.alph
            .letters(msg)
            .map_cp(self.alph, |cp| {
                let (a, b) = &self.key[idx % self.period()];
                idx += 1;
                a.num() * cp + b.cp()
            })
            .chars(self.alph)
            .collect()
    }
    fn decrypt(&self, msg: &str) -> String {
        let mut idx = 0;
        self.alph
            .letters(msg)
            .map_cp(self.alph, |cp| {
                let (a, b) = &self.key[idx % self.period()];
                idx += 1;
                a.inv() * (cp - b.cp())
            })
            .chars(self.alph)
            .collect()
    }
}
impl<'l> Solve<'l> for PeriodicAffine<'l> {
    type Options = PeriodicAffineSolve;

    fn solve(lang: &'l Lang, msg: &str, options: Self::Options) -> Self {
        let alph = Self::alph(lang);
        let ct: Vec<i32> = alph.code_points(msg).collect();
        let mut best_score = f32::MIN;
        let mut best_key = vec![(1, 0)];

        for period in 2.min(msg.len())..options.max_period.min(msg.len()) {
            let mut period_key = vec![(1, 0); period];

            for (start, k) in period_key.iter_mut().enumerate() {
                let mut best_col_score = f32::MIN;

                for affine in Affine::key_space(lang, ()) {
                    let inv = affine.a().inv();
                    let b = affine.b().cp();

                    let col_score = alph.score(
                        (start..ct.len())
                            .step_by(period)
                            .map(|i| util::modulo(inv * (ct[i] - b), alph.alphabet_len())),
                        StatsSize::Unigrams,
                    );

                    if col_score > best_col_score {
                        best_col_score = col_score;
                        *k = (affine.a().num(), b);
                    }
                }
            }

            let cipher = PeriodicAffine::new(lang, period_key.clone()).unwrap();
            let period_score = alph.score_str(&cipher.decrypt(msg), options.stats_size);

            if period_score > best_score {
                best_key = period_key;
                best_score = period_score;
            }
        }

        PeriodicAffine::new(lang, best_key).unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::common::LANG;

    #[test]
    fn encrypt_and_decrypt() {
        let key = vec![(7, 12), (5, 2), (19, 19)];
        let cipher = PeriodicAffine::new(&LANG, key).unwrap();

        let plaintext =
            "Why, sometimes I've believed as many as six impossible things before breakfast.";
        let ciphertext = &cipher.encrypt(plaintext);

        assert_eq!(
            ciphertext,
            "Klh, iunotpswx Q'dr twuqwcor ti ktzs ti opr qnnuxiqmlw qjqgco mobzbw mbwtebtit."
        );
        assert_eq!(plaintext, cipher.decrypt(ciphertext));
    }

    #[test]
    fn solve() {
        let key = vec![(7, 12), (5, 2)];
        let cipher = PeriodicAffine::new(&LANG, key.clone()).unwrap();

        let plaintext =
            "Why, sometimes I've believed as many as six impossible things before breakfast.";
        let ciphertext = cipher.encrypt(plaintext);

        let PeriodicAffine { key: k, .. } =
            PeriodicAffine::solve(&LANG, &ciphertext, Default::default());

        assert!(k
            .into_iter()
            .map(|(coprime, cp)| (coprime.num(), cp.cp()))
            .eq(key));
    }
}
