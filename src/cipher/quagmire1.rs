//! See the documentation for [`Quagmire1`]

use crate::{
    analysis::{self, stream, substitution},
    cipher::Cipher,
    key::{Key, KeyFrom, KeyResult, Keyword, SubAlph},
    lang::{
        with_alphabet::{StatsSize, WithAlphabet},
        AlphabetLen, Lang,
    },
    prelude::{DictionaryAttack, Solve},
    util,
};
use rayon::prelude::*;

/// The [`Options`](Solve::Options) type for [`Quagmire1`]. Used to
/// configure of [`Solve::solve`].
pub struct Quagmire1Solve {
    /// The [`StatsSize`] that should be used for scoring.
    pub stats_size: StatsSize,
    /// The maximum period to search.
    pub max_period: usize,
    /// The maximum number of iterations that the algorithm should
    /// run for.
    pub max_iterations: usize,
    /// The minimum number of times that a solution must be reached
    /// in order for it to be assumed as the best solution.
    pub min_repetitions: usize,
    /// The minimum IOC to consider solving as a substitution.
    pub min_ioc: f32,
}
impl Default for Quagmire1Solve {
    fn default() -> Self {
        Self {
            stats_size: StatsSize::default(),
            max_period: 25,
            max_iterations: 2_000,
            min_repetitions: 5,
            min_ioc: 0.055,
        }
    }
}

/// Quagmire I is a polyalphabetic cipher that uses two keywords: one to
/// key a plaintext alphabet, and another to shift the ciphertext
/// alphabet for each period. It can be solved by first finding the shifts,
/// then breaking as a monoalphabetic substitution.
#[derive(Debug)]
pub struct Quagmire1<'l> {
    alph: WithAlphabet<'l>,
    plain_sub: SubAlph<'l>,
    cipher_kw: Keyword<'l>,
}

impl<'l> Quagmire1<'l> {
    /// Creates a new [`Quagmire1`] cipher from the plaintext alphabet
    /// keyword and the ciphertext alphabet keyword.
    pub fn new<A, K>(lang: &'l Lang, plain_alph: A, cipher_kw: K) -> KeyResult<Self>
    where
        SubAlph<'l>: KeyFrom<'l, A>,
        Keyword<'l>: KeyFrom<'l, K>,
    {
        Ok(Self {
            alph: Self::alph(lang),
            plain_sub: SubAlph::key_from(Self::alph(lang), plain_alph)?,
            cipher_kw: Keyword::key_from(Self::alph(lang), cipher_kw)?,
        })
    }
    /// Gets the plaintext keyed alphabet.
    pub fn plain_alph(&self) -> &SubAlph {
        &self.plain_sub
    }
    /// Gets the keyword for the ciphertext alphabets.
    pub fn keyword(&self) -> &Keyword {
        &self.cipher_kw
    }
}
impl<'l> Cipher<'l> for Quagmire1<'l> {
    fn alphabet_len() -> AlphabetLen {
        AlphabetLen::Any
    }
    fn random(lang: &'l Lang) -> Self {
        Self {
            alph: Self::alph(lang),
            plain_sub: SubAlph::random(Self::alph(lang)),
            cipher_kw: Keyword::random(Self::alph(lang)),
        }
    }
    fn default(lang: &'l Lang) -> Self {
        Self {
            alph: Self::alph(lang),
            plain_sub: SubAlph::default(Self::alph(lang)),
            cipher_kw: Keyword::default(Self::alph(lang)),
        }
    }
    fn encrypt(&self, msg: &str) -> String {
        let a_pos = self.plain_sub.decrypt(0);

        stream::with(msg, self.alph, &self.cipher_kw, |cp, key| {
            self.plain_sub.decrypt(cp) - a_pos + key
        })
    }
    fn decrypt(&self, msg: &str) -> String {
        let a_pos = self.plain_sub.decrypt(0);

        stream::with(msg, self.alph, &self.cipher_kw, |cp, key| {
            self.plain_sub
                .encrypt(util::modulo(cp + a_pos - key, self.alph.alphabet_len()))
        })
    }
}
impl<'l> Solve<'l> for Quagmire1<'l> {
    type Options = Quagmire1Solve;

    fn solve(lang: &'l Lang, msg: &str, options: Self::Options) -> Self {
        let alph = Self::alph(lang);
        let ct: Vec<i32> = alph.code_points(msg).collect();

        (2..options.max_period.min(msg.len()))
            .into_par_iter()
            .filter_map(|period| {
                let expected = &analysis::frequencies(&analysis::counts(
                    (0..ct.len()).step_by(period).map(|idx| ct[idx] as usize),
                    StatsSize::Unigrams,
                ))[0..alph.alphabet_len() as usize];

                let mut shifts = vec![0];

                for col in 1..period {
                    let mut best_shift = 0;
                    let mut best_mono = f32::MIN;

                    for shift in 0..alph.alphabet_len() {
                        let freqs = &analysis::frequencies(&analysis::counts(
                            (col..ct.len()).step_by(period).map(|idx| {
                                util::modulo(ct[idx] - shift, alph.alphabet_len()) as usize
                            }),
                            StatsSize::Unigrams,
                        ))[0..alph.alphabet_len() as usize];

                        let mono = analysis::scalar_product(freqs, expected);

                        if mono > best_mono {
                            best_mono = mono;
                            best_shift = shift;
                        }
                    }

                    shifts.push(best_shift);
                }

                // shift each column to obtain a ciphertext that can be solved as a monoalphabetic
                // substitution.
                let ct: Vec<usize> = shifts
                    .iter()
                    .cycle()
                    .zip(&ct)
                    .map(|(&shift, &cp)| util::modulo(cp - shift, alph.alphabet_len()) as usize)
                    .collect();

                if analysis::ioc(ct.iter().copied(), StatsSize::Unigrams) < options.min_ioc {
                    None
                } else {
                    let (_, key) = substitution::solve_hill_climbing(
                        &ct,
                        alph,
                        options.max_iterations,
                        options.min_repetitions,
                        options.stats_size,
                    );

                    // recover the keywords.
                    let plain_kw = util::invert(&key);
                    let cipher_kw: Vec<i32> = shifts
                        .iter()
                        .map(|&s| util::modulo(key[0] + s, alph.alphabet_len()))
                        .collect();

                    let cipher = Quagmire1::new(lang, &plain_kw[..], &cipher_kw[..]).unwrap();
                    let score = alph.score_str(&cipher.decrypt(msg), options.stats_size);

                    Some((score, cipher))
                }
            })
            .max_by(|(s1, _), (s2, _)| s1.partial_cmp(s2).unwrap())
            .map(|(_, cipher)| cipher)
            .unwrap_or_else(|| Self::default(lang))
    }
}
impl<'l, 'kw> DictionaryAttack<'l, 'kw> for Quagmire1<'l> {
    type Options = Quagmire1DictionaryOptions;

    fn dictionary_attack(
        lang: &'l Lang,
        msg: &str,
        keywords: impl Iterator<Item = &'kw str> + Send,
        options: Self::Options,
    ) -> Self {
        // find all keywords of required length.
        let alph = Self::alph(lang);
        let len = alph.alphabet_len();
        let kw: Vec<Vec<i32>> = keywords
            .map(|kw| alph.code_points(kw).collect::<Vec<i32>>())
            .filter(|kw| (0..=options.max_keyword_len).contains(&kw.len()))
            .take(options.max_keywords)
            .collect();
        let ct: Vec<i32> = alph.code_points(msg).collect();

        let best_key = kw
            .par_iter()
            .map(|plain_kw| {
                let mut best_score = f32::MIN;
                let mut best_key = None;

                let mut test_alphabet = |plain_alphabet: Vec<i32>| {
                    let a_pos = (0..len as usize)
                        .find(|&i| plain_alphabet[i] == 0)
                        .unwrap_or(0) as i32;

                    for cipher_kw in kw.iter() {
                        let score = alph.score(
                            ct.iter().zip(cipher_kw.iter().cycle()).map(|(&cp, &key)| {
                                plain_alphabet[util::modulo(cp + a_pos - key, len) as usize]
                            }),
                            options.stats_size,
                        );

                        if score > best_score {
                            best_score = score;
                            best_key = Some((plain_alphabet.clone(), cipher_kw));
                        }
                    }
                };

                test_alphabet(util::fill_continue(plain_kw, len as usize));
                test_alphabet(util::fill_continue_max(plain_kw, len as usize));
                test_alphabet(util::fill_start(plain_kw, len as usize));

                (best_score, best_key)
            })
            .max_by(|(s1, _), (s2, _)| s1.partial_cmp(s2).unwrap())
            .and_then(|(_, x)| x);

        best_key
            .and_then(|(plain_kw, cipher_kw)| Self::new(lang, &plain_kw[..], &cipher_kw[..]).ok())
            .unwrap_or_else(|| Self::default(lang))
    }
}

/// Options for configuring the Quagmire1 dictionary attack.
pub struct Quagmire1DictionaryOptions {
    /// Minimum keyword length, filters the provided iterator.
    pub min_keyword_len: usize,
    /// Maximum keyword length, filters the provided iterator.
    pub max_keyword_len: usize,
    /// Maximum number of keywords to try, the algorithm is quadratic so
    /// this number has a significant effect.
    pub max_keywords: usize,
    /// StatsSize for scoring decryptions.
    pub stats_size: StatsSize,
}

impl Default for Quagmire1DictionaryOptions {
    fn default() -> Self {
        Self {
            min_keyword_len: 3,
            max_keyword_len: 5,
            max_keywords: 1_000,
            stats_size: StatsSize::Quadgrams,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::common::LANG;

    #[test]
    fn encrypt_and_decrypt() {
        let cipher = Quagmire1::new(&LANG, "quagmire", "keyword").unwrap();

        let plaintext =
            "Why, sometimes I've believed as many as six impossible things before breakfast.";
        let ciphertext = &cipher.encrypt(plaintext);

        assert_eq!(
            ciphertext,
            "Blr, katizhabc U'tp zdgrwtpb yk qrod em krj gmrkkcuyuj ndrcey zdcavi fidwxxdyt.",
        );
        assert_eq!(&cipher.decrypt(ciphertext), plaintext)
    }
}
