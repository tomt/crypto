//! See the documentation for [`ClassicVigenere`].

use crate::{
    prelude::*,
    solve::{
        brute_force::{KeySpace, MaxPeriod},
        periodic, Solve,
    },
};

use self::util::Keyword;

/// The [`Options`](Solve::Options) type for [`ClassicVigenere`]. Used to
/// configure [`Solve::solve`].
pub struct ClassicVigenereSolve {
    /// The [`StatsSize`] that should be used for scoring.
    pub stats_size: StatsSize,
    /// The maximum keyword length that should be tested.
    pub max_key_length: usize,
}

impl Default for ClassicVigenereSolve {
    fn default() -> Self {
        Self {
            stats_size: StatsSize::default(),
            max_key_length: 30,
        }
    }
}

/// The Classic Vigenere cipher is a polyalphabetic substitution which
/// uses a keyword to perform a caesar shift to each column. The text
/// is divided up into as many columns as there are key letters, then
/// each column is shifted by the corresponding key letter. Since each
/// column is a caesar shift, this cipher and others of its type can be
/// identified by taking the average index of coincedence over a column.
/// This cipher is solved by trying possible key lengths then varying
/// each key column in turn.
#[derive(Debug)]
pub struct ClassicVigenere {
    keyword: Keyword,
}

impl Identity for ClassicVigenere {
    fn identity(ctx: &Context) -> Self {
        Self {
            keyword: Keyword::identity(ctx),
        }
    }
}

impl Cipher for ClassicVigenere {
    type Error = ();

    fn encrypt(&self, ctx: &Context, msg: Msg) -> Result<Msg, Self::Error> {
        Ok(msg.add(&self.keyword).modulo(ctx.alphabet_len))
    }

    fn decrypt(&self, ctx: &Context, msg: Msg) -> Result<Msg, Self::Error> {
        Ok(msg.subtract(&self.keyword).modulo(ctx.alphabet_len))
    }
}

impl Solve for ClassicVigenere {
    type Options = ClassicVigenereSolve;

    fn solve(ctx: &Context, lang: &Lang, msg: &Msg, options: Self::Options) -> Self {
        let len = ctx.alphabet_len as i32;
        let (_, keyword) = periodic::solve_vig(
            msg,
            lang,
            options.stats_size,
            |key_cp, cipher_cp, _| util::modulo(cipher_cp - key_cp, len),
            1,
            options.max_key_length,
        );

        Self {
            keyword: Keyword(Msg {
                alphabet_len: ctx.alphabet_len,
                data: keyword,
            }),
        }
    }
}

impl KeySpace for ClassicVigenere {
    type Iter = ClassicVigenereKeySpace;
    type Limit = MaxPeriod;

    fn key_space(ctx: &Context, MaxPeriod(max_len): Self::Limit) -> Self::Iter {
        ClassicVigenereKeySpace {
            max_len,
            curr: Some(Self::identity(ctx)),
        }
    }
}

// impl<'l> CribMatches<'l> for ClassicVigenere<'l> {
//     fn reconstruct_key(
//         lang: &'l Lang,
//         pos: usize,
//         crib: &[i32],
//         ciphertext: &[i32],
//     ) -> Option<Self> {
//         let alph = Self::alph(lang);
//         let shifts: Vec<i32> = crib
//             .iter()
//             .zip(ciphertext)
//             .map(|(&a, &b)| util::modulo(b - a, alph.alphabet_len()))
//             .collect();

//         'outer: for period in 3..(crib.len() + 1) / 2 {
//             for col in 0..period {
//                 for i in (period + col..crib.len()).step_by(period) {
//                     if shifts[i] != shifts[col] {
//                         continue 'outer;
//                     }
//                 }
//             }

//             let mut key = vec![0; period];

//             for i in 0..period {
//                 key[(i + pos) % period] = shifts[i];
//             }

//             return Some(ClassicVigenere::new(lang, &key[..]).unwrap());
//         }

// default        None
//     }
// }

// impl<'l, 'kw> FromKeyword<'l, 'kw> for ClassicVigenere<'l> {
//     type Iter = std::iter::Once<ClassicVigenere<'l>>;
//     type Variants = ();

//     fn from_keyword(lang: &'l Lang, keyword: &'kw str, _: Self::Variants) -> Self::Iter {
//         std::iter::once(Self::new(lang, keyword).unwrap())
//     }
// }

/// An iterator over the key space of [`ClassicVigenere`].
pub struct ClassicVigenereKeySpace {
    max_len: usize,
    curr: Option<ClassicVigenere>,
}

impl Iterator for ClassicVigenereKeySpace {
    type Item = ClassicVigenere;

    fn next(&mut self) -> Option<Self::Item> {
        let curr = self.curr.take()?;
        // self.curr = curr
        //     .keyword
        //     .clone()
        //     .successor(curr.alph)
        //     .filter(|kw| kw.len() <= self.max_len)
        //     .map(|keyword| ClassicVigenere {
        //         alph: curr.alph,
        //         keyword,
        //     });

        // Some(curr)
        todo!()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::common::*;

    #[test]
    fn encrypt_and_decrypt() {
        let keyword = Keyword(TEXT_MAP.msg("vigenere"));
        let cipher = ClassicVigenere { keyword };

        let plaintext =
            "Why, sometimes I've believed as many as six impossible things before breakfast.";
        let ciphertext =
            "Rpe, wbqvxdukw V'zv fztoiiiu en ugrl ej wdf oqcsjwdjri glzrba hissii wzkexjrwo.";

        test_encrypt_decrypt(&cipher, plaintext, ciphertext);
    }

    #[test]
    fn solve() {
        let key = [10, 4, 24];

        let keyword = Keyword(Msg {
            alphabet_len: CONTEXT.alphabet_len,
            data: Vec::from(&key),
        });
        let cipher = ClassicVigenere { keyword };

        let plaintext =
            "Why, sometimes I've believed as many as six impossible things before breakfast.";
        let ciphertext = encrypt_str(&cipher, plaintext);

        let ClassicVigenere { keyword, .. } = ClassicVigenere::solve(
            CONTEXT,
            &LANG,
            &TEXT_MAP.msg(&ciphertext),
            Default::default(),
        );

        assert_eq!(keyword.0.data, key);
    }
}
