//! See the documentation for [`Substitution`]

use crate::{
    prelude::*,
    solve::{substitution, Solve},
};

use self::util::Alphabet;

/// The [`Options`](Solve::Options) type for [`Substitution`]. Used to
/// configure aspects of [`Solve::solve`].
pub struct SubstitutionSolve {
    /// The [`StatsSize`] that should be used for scoring.
    pub stats_size: StatsSize,
    /// The maximum number of iterations that the algorithm should
    /// run for.
    pub max_iterations: usize,
    /// The minimum number of times that a solution must be reached
    /// in order for it to be assumed as the best solution.
    pub min_repetitions: usize,
}
impl Default for SubstitutionSolve {
    fn default() -> Self {
        Self {
            stats_size: StatsSize::default(),
            max_iterations: 2000,
            min_repetitions: 5,
        }
    }
}

/// The Substitution cipher uses a plaintext and ciphertext alphabet, then
/// encodes each plaintext letter to the corresponding ciphertext one. It
/// can be keyed with a keyword which is used to generate the substitution
/// alphabet. The cipher performs a monoalphabetic substitution, and can be
/// solved by incrementally changing the key to gradually approach an optimal
/// solution. This implementation can take an alphabet of any length.
#[derive(Debug)]
pub struct Substitution {
    sub: Alphabet,
}

impl Cipher for Substitution {
    type Error = ();

    fn encrypt(&self, _: &Context, msg: Msg) -> Result<Msg, Self::Error> {
        Ok(msg.sub(|cp| self.sub.forward(cp)))
    }

    fn decrypt(&self, _: &Context, msg: Msg) -> Result<Msg, Self::Error> {
        Ok(msg.sub(|cp| self.sub.backward(cp)))
    }
}

impl Solve for Substitution {
    type Options = SubstitutionSolve;

    fn solve(_: &Context, lang: &Lang, msg: &Msg, solve: Self::Options) -> Self {
        let (_, key) = substitution::solve_hill_climbing(
            msg,
            lang,
            solve.max_iterations,
            solve.min_repetitions,
            solve.stats_size,
        );

        Self {
            sub: Alphabet::new(key),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::common::*;

    #[test]
    fn encrypt_and_decrypt() {
        let keyword = &TEXT_MAP.msg("keyword").data;
        let sub = Alphabet::new(util::fill_start(keyword, CONTEXT.alphabet_len));
        let cipher = Substitution { sub };

        let plaintext =
            "Why, sometimes I've believed as many as six impossible things before breakfast.";
        let ciphertext =
            "Uax, pjhoqbhop B'to eogbotow kp hkix kp pbv bhljppbego qabidp eorjno enokfrkpq.";

        test_encrypt_decrypt(&cipher, plaintext, ciphertext);
    }
}
