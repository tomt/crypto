//! See the documentation for [`Quagmire2`].

use crate::{
    analysis::stream,
    cipher::Cipher,
    key::{Key, KeyFrom, KeyResult, Keyword, SubAlph},
    lang::{
        with_alphabet::{StatsSize, WithAlphabet},
        AlphabetLen, Lang,
    },
    prelude::DictionaryAttack,
    util,
};
use rayon::prelude::*;

/// Quagmire 2 uses an unkeyed (ABCD..XYZ) plaintext alphabet
/// and several keyed ciphertext alphabets. The ciphertext
/// alphabet is constructed from a keyword, then shifted so that
/// the first letter of each ciphertext alphabet is the same as
/// the corresponding key letter.
#[derive(Debug)]
pub struct Quagmire2<'l> {
    alph: WithAlphabet<'l>,
    cipher_sub: SubAlph<'l>,
    cipher_kw: Keyword<'l>,
}

impl<'l> Quagmire2<'l> {
    /// Creates a new [`Quagmire2`] cipher from the ciphertext alphabet
    /// keyword and the ciphertext alphabet shift keyword.
    pub fn new<A, K>(lang: &'l Lang, cipher_alph: A, cipher_kw: K) -> KeyResult<Self>
    where
        SubAlph<'l>: KeyFrom<'l, A>,
        Keyword<'l>: KeyFrom<'l, K>,
    {
        Ok(Self {
            alph: Self::alph(lang),
            cipher_sub: SubAlph::key_from(Self::alph(lang), cipher_alph)?,
            cipher_kw: Keyword::key_from(Self::alph(lang), cipher_kw)?,
        })
    }
    /// Gets the plaintext keyed alphabet.
    pub fn cipher_alph(&self) -> &SubAlph {
        &self.cipher_sub
    }
    /// Gets the keyword for the ciphertext alphabets.
    pub fn keyword(&self) -> &Keyword {
        &self.cipher_kw
    }
}
impl<'l> Cipher<'l> for Quagmire2<'l> {
    fn alphabet_len() -> AlphabetLen {
        AlphabetLen::Any
    }
    fn random(lang: &'l Lang) -> Self {
        Self {
            alph: Self::alph(lang),
            cipher_sub: SubAlph::random(Self::alph(lang)),
            cipher_kw: Keyword::random(Self::alph(lang)),
        }
    }
    fn default(lang: &'l Lang) -> Self {
        Self {
            alph: Self::alph(lang),
            cipher_sub: SubAlph::default(Self::alph(lang)),
            cipher_kw: Keyword::default(Self::alph(lang)),
        }
    }
    fn encrypt(&self, msg: &str) -> String {
        stream::with(msg, self.alph, &self.cipher_kw, |cp, key| {
            self.cipher_sub.encrypt(util::modulo(
                self.cipher_sub.decrypt(key) + cp,
                self.alph.alphabet_len(),
            ))
        })
    }
    fn decrypt(&self, msg: &str) -> String {
        stream::with(msg, self.alph, &self.cipher_kw, |cp, key| {
            self.cipher_sub.decrypt(cp) - self.cipher_sub.decrypt(key)
        })
    }
}
impl<'l, 'kw> DictionaryAttack<'l, 'kw> for Quagmire2<'l> {
    type Options = Quagmire2DictionaryOptions;

    fn dictionary_attack(
        lang: &'l Lang,
        msg: &str,
        keywords: impl Iterator<Item = &'kw str> + Send,
        options: Self::Options,
    ) -> Self {
        // find all keywords of required length.
        let alph = Self::alph(lang);
        let len = alph.alphabet_len();
        let kw: Vec<Vec<i32>> = keywords
            .map(|kw| alph.code_points(kw).collect::<Vec<i32>>())
            .filter(|kw| (options.min_keyword_len..=options.max_keyword_len).contains(&kw.len()))
            .take(options.max_keywords)
            .collect();
        let ct: Vec<i32> = alph.code_points(msg).collect();

        let best_key = kw
            .par_iter()
            .map(|cipher_alph_kw| {
                let mut best_score = f32::MIN;
                let mut best_key = None;
                let mut inv = vec![0; len as usize];

                let mut test_alphabet = |cipher_alphabet: Vec<i32>| {
                    util::invert_into(&cipher_alphabet, &mut inv);

                    for cipher_kw in kw.iter() {
                        let score = alph.score(
                            ct.iter().zip(cipher_kw.iter().cycle()).map(|(&cp, &key)| {
                                util::modulo(inv[cp as usize] - inv[key as usize], len)
                            }),
                            options.stats_size,
                        );

                        if score > best_score {
                            best_score = score;
                            best_key = Some((cipher_alphabet.clone(), cipher_kw));
                        }
                    }
                };

                test_alphabet(util::fill_continue(cipher_alph_kw, len as usize));
                test_alphabet(util::fill_continue_max(cipher_alph_kw, len as usize));
                test_alphabet(util::fill_start(cipher_alph_kw, len as usize));

                (best_score, best_key)
            })
            .max_by(|(s1, _), (s2, _)| s1.partial_cmp(s2).unwrap())
            .and_then(|(_, x)| x);

        best_key
            .and_then(|(cipher_alph_kw, cipher_kw)| {
                Self::new(lang, &cipher_alph_kw[..], &cipher_kw[..]).ok()
            })
            .unwrap_or_else(|| Self::default(lang))
    }
}

/// Options for configuring the Quagmire2 dictionary attack.
pub struct Quagmire2DictionaryOptions {
    /// Minimum keyword length, filters the provided iterator.
    pub min_keyword_len: usize,
    /// Maximum keyword length, filters the provided iterator.
    pub max_keyword_len: usize,
    /// Maximum number of keywords to try, the algorithm is quadratic so
    /// this number has a significant effect.
    pub max_keywords: usize,
    /// StatsSize for scoring decryptions.
    pub stats_size: StatsSize,
}

impl Default for Quagmire2DictionaryOptions {
    fn default() -> Self {
        Self {
            min_keyword_len: 3,
            max_keyword_len: 5,
            max_keywords: 1_000,
            stats_size: StatsSize::Quadgrams,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::common::LANG;

    #[test]
    fn encrypt_and_decrypt() {
        let cipher = Quagmire2::new(&LANG, "quagmire", "keyword").unwrap();

        let plaintext =
            "Why, sometimes I've believed as many as six impossible things before breakfast.";
        let ciphertext = &cipher.encrypt(plaintext);

        assert_eq!(
            ciphertext,
            "Eow, kavgmpebr O'xp fdmzjxpj yk qrlh en kzg ebzhkroqzk oqzwig fdcabg lcdwckdgq."
        );
        assert_eq!(&cipher.decrypt(ciphertext), plaintext)
    }
}
