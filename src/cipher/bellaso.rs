//! See the documentation for [`Bellaso`].

use crate::{
    prelude::*,
    solve::{
        brute_force::{KeySpace, MaxPeriod},
        periodic, Solve,
    },
};
use util::Keyword;

/// The [`Options`](Solve::Options) type for [`Bellaso`]. Used to
/// configure [`Solve::solve`].
pub struct BellasoSolve {
    /// The [`StatsSize`] that should be used for scoring.
    pub stats_size: StatsSize,
    /// The maximum keyword length that should be tested.
    pub max_key_length: usize,
}

impl Default for BellasoSolve {
    fn default() -> Self {
        Self {
            stats_size: StatsSize::default(),
            max_key_length: 30,
        }
    }
}

/// A periodic, polyalphabetic cipher that is reciprocal and uses the following
/// tableau:
///
/// ```md
///     a b c d e f g h i j k l m n o p q r s t u v w x y z
/// -------------------------------------------------------
/// A | N O P Q R S T U V W X Y Z A B C D E F G H I J K L M
/// B | Z N O P Q R S T U V W X Y B C D E F G H I J K L M A
/// C | Y Z N O P Q R S T U V W X C D E F G H I J K L M A B
/// D | X Y Z N O P Q R S T U V W D E F G H I J K L M A B C
/// E | W X Y Z N O P Q R S T U V E F G H I J K L M A B C D
/// F | V W X Y Z N O P Q R S T U F G H I J K L M A B C D E
/// G | U V W X Y Z N O P Q R S T G H I J K L M A B C D E F
/// H | T U V W X Y Z N O P Q R S H I J K L M A B C D E F G
/// I | S T U V W X Y Z N O P Q R I J K L M A B C D E F G H
/// J | R S T U V W X Y Z N O P Q J K L M A B C D E F G H I
/// K | Q R S T U V W X Y Z N O P K L M A B C D E F G H I J
/// L | P Q R S T U V W X Y Z N O L M A B C D E F G H I J K
/// M | O P Q R S T U V W X Y Z N M A B C D E F G H I J K L
/// N | M L K J I H G F E D C B A Z Y X W V U T S R Q P O N
/// O | A M L K J I H G F E D C B Y X W V U T S R Q P O N Z
/// P | B A M L K J I H G F E D C X W V U T S R Q P O N Z Y
/// Q | C B A M L K J I H G F E D W V U T S R Q P O N Z Y X
/// R | D C B A M L K J I H G F E V U T S R Q P O N Z Y X W
/// S | E D C B A M L K J I H G F U T S R Q P O N Z Y X W V
/// T | F E D C B A M L K J I H G T S R Q P O N Z Y X W V U
/// U | G F E D C B A M L K J I H S R Q P O N Z Y X W V U T
/// V | H G F E D C B A M L K J I R Q P O N Z Y X W V U T S
/// W | I H G F E D C B A M L K J Q P O N Z Y X W V U T S R
/// X | J I H G F E D C B A M L K P O N Z Y X W V U T S R Q
/// Y | K J I H G F E D C B A M L O N Z Y X W V U T S R Q P
/// Z | L K J I H G F E D C B A M N Z Y X W V U T S R Q P O
/// ```
#[derive(Debug)]
pub struct Bellaso {
    keyword: Keyword,
}

impl Bellaso {
    /// Simulates the operation of the tableau of the bellaso cipher.
    fn bellaso(plain: i32, key: i32, alphabet_len: i32) -> i32 {
        let half = alphabet_len / 2;

        match (plain < half, key < half) {
            (true, true) => util::modulo(plain - key, half) + half,
            (true, false) => util::modulo(key - plain - 1, half),
            (false, true) => util::modulo(plain + key, half),
            (false, false) => util::modulo(-1 - plain - key, half) + half,
        }
    }
}

impl Symmetric for Bellaso {
    type Error = ();

    fn run(&self, ctx: &Context, msg: Msg) -> Result<Msg, Self::Error> {
        Ok(msg.periodic(self.keyword.period(), |col| {
            move |cp| Bellaso::bellaso(cp, self.keyword[col], ctx.alphabet_len as i32)
        }))
    }
}

impl Solve for Bellaso {
    type Options = BellasoSolve;

    fn solve(ctx: &Context, lang: &Lang, msg: &Msg, options: Self::Options) -> Self {
        let (_, keyword) = periodic::solve_vig(
            msg,
            lang,
            options.stats_size,
            |key_cp, cipher_cp, _| Bellaso::bellaso(cipher_cp, key_cp, ctx.alphabet_len as i32),
            1,
            options.max_key_length,
        );

        Self {
            keyword: Keyword(Msg {
                alphabet_len: ctx.alphabet_len,
                data: keyword,
            }),
        }
    }
}

impl KeySpace for Bellaso {
    type Iter = BellasoKeySpace;
    type Limit = MaxPeriod;

    fn key_space(_: &Context, MaxPeriod(max_len): Self::Limit) -> Self::Iter {
        BellasoKeySpace {
            max_len,
            curr: Keyword::default(),
        }
    }
}

// impl<'l> CribMatches<'l> for Bellaso<'l> {
//     fn reconstruct_key(
//         lang: &'l Lang,
//         pos: usize,
//         crib: &[i32],
//         ciphertext: &[i32],
//     ) -> Option<Self> {
//         let alph = Self::alph(lang);
//         let len = alph.alphabet_len();
//         let shifts: Vec<i32> = crib
//             .iter()
//             .zip(ciphertext)
//             .map(|(&plain, &cipher)| {
//                 { (0..len).find(|&key| Bellaso::bellaso(plain, key, len) == cipher) }.unwrap()
//             })
//             .collect();

//         'outer: for period in 3..(crib.len() + 1) / 2 {
//             for col in 0..period {
//                 for i in (period + col..crib.len()).step_by(period) {
//                     if shifts[i] != shifts[col] {
//                         continue 'outer;
//                     }
//                 }
//             }

//             let mut key = vec![0; period];

//             for i in 0..period {
//                 key[(i + pos) % period] = shifts[i];
//             }

//             return Some(Bellaso::new(lang, &key[..]).unwrap());
//         }

//         None
//     }
// }

// impl<'l, 'kw> FromKeyword<'l, 'kw> for Bellaso<'l> {
//     type Iter = std::iter::Once<Bellaso<'l>>;
//     type Variants = ();

//     fn from_keyword(lang: &'l Lang, keyword: &'kw str, _: Self::Variants) -> Self::Iter {
//         std::iter::once(Self::new(lang, keyword).unwrap())
//     }
// }

/// An iterator over the key space of [`Bellaso`].
pub struct BellasoKeySpace {
    max_len: usize,
    curr: Keyword,
}

impl Iterator for BellasoKeySpace {
    type Item = Bellaso;

    fn next(&mut self) -> Option<Self::Item> {
        if self.curr.len() > self.max_len {
            return None;
        }

        todo!()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::common::*;

    #[test]
    fn encrypt_and_decrypt() {
        let keyword = Keyword(TEXT_MAP.msg("bellaso"));
        let cipher = Bellaso { keyword };

        let plaintext =
            "Why, sometimes I've believed as many as six impossible things before breakfast.";
        let ciphertext =
            "Kqj, dbfjhrotf J'qq xtnvaqqz pd zeym wd dvx fygmdfjmxn ewvuhg xtubqj nitpxmagk.";

        test_encrypt_decrypt(&cipher, plaintext, ciphertext);
    }

    #[test]
    fn solve() {
        let key = [10, 4, 24];

        let cipher = Bellaso {
            keyword: Keyword(Msg {
                alphabet_len: CONTEXT.alphabet_len,
                data: Vec::from(&key),
            }),
        };

        let plaintext =
            "Why, sometimes I've believed as many as six impossible things before breakfast.";
        let ciphertext = cipher.encrypt(CONTEXT, TEXT_MAP.msg(plaintext)).unwrap();

        let Bellaso { keyword } = Bellaso::solve(
            CONTEXT,
            &LANG,
            &ciphertext,
            BellasoSolve {
                max_key_length: 4,
                ..default()
            },
        );

        assert_eq!(keyword.0.data, key);
    }
}
