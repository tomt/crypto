use crate::{
    analysis::{brute_force::KeySpace, Solve, transposition::Transposition},
    cipher::Cipher,
    key::Keyword,
    prelude::WithAlphabet, util,
};

/// TODO
pub struct BlockTransposition<'l> {
    alph: WithAlphabet<'l>,
    keyword: Keyword<'l>,
}

impl<'l> BlockTransposition<'l> {

}

impl<'l> Transposition for BlockTransposition<'l> {
    fn decrypt_indices(&self, len: usize, buf: &mut Vec<usize>) {
        let permutation = util::order(self.keyword.word());
        let key_len = permutation.len();
        let column_len = len / key_len;

        for col in 0..key_len {
            for row in 0..column_len {
                buf[row * key_len + permutation[col]] = row * key_len + col;
            }
        }
    }
}

