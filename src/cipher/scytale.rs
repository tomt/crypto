//! See the documentation for [`Scytale`].

use crate::{
    analysis::{
        brute_force::KeySpace,
        transposition::{self, Transposition},
    },
    cipher::Cipher,
    key::{Integer, Key, KeyFrom, KeyResult, Successor},
    lang::{with_alphabet::WithAlphabet, AlphabetLen, Lang},
};

type Faces = Integer<2, 98>;

/// Scytale is a transposition cipher which was performed with a
/// rod whose cross section was a regular n-sided polygon. A piece
/// of paper was wrapped around the rod, and a message was written
/// on the paper going left to right. The paper would then be unwrapped,
/// and the resulting message was the ciphertext. To decrypt, the paper
/// would simply be wrapped around a similar rod and read from. This
/// implementation ignores punctuation and only transposes the letters,
/// adding back the punctuation once encrypted/decrypted. Since the
/// only key variable is the number of faces, this value can simply
/// be brute-forced to find a solution. Values from 2 to 100 are tried,
/// and these are the limits for inputs to the cipher. This cipher
/// can take any alphabet.
#[derive(Debug)]
pub struct Scytale<'l> {
    alph: WithAlphabet<'l>,
    faces: Faces,
}

impl<'l> Scytale<'l> {
    /// Creates a new Scytale cipher, with `faces` as the key. `faces`
    /// can be any value in the range `2..=100`.
    pub fn new<F>(lang: &'l Lang, faces: F) -> KeyResult<Self>
    where
        Faces: KeyFrom<'l, F>,
    {
        Ok(Self {
            alph: Self::alph(lang),
            faces: Faces::key_from(Self::alph(lang), faces)?,
        })
    }
    /// Gets the number of faces.
    pub fn faces(&self) -> &Faces {
        &self.faces
    }
}
impl<'l> Cipher<'l> for Scytale<'l> {
    fn alphabet_len() -> AlphabetLen {
        AlphabetLen::Any
    }
    fn random(lang: &'l Lang) -> Self {
        Self {
            alph: Self::alph(lang),
            faces: Faces::random(Self::alph(lang)),
        }
    }
    fn default(lang: &'l Lang) -> Self {
        Self {
            alph: Self::alph(lang),
            faces: Integer::default(Self::alph(lang)),
        }
    }
    fn encrypt(&self, msg: &str) -> String {
        transposition::encrypt(msg, self.alph, self)
    }
    fn decrypt(&self, msg: &str) -> String {
        transposition::decrypt(msg, self.alph, self)
    }
}
impl<'l> Transposition for Scytale<'l> {
    fn decrypt_indices(&self, len: usize, buf: &mut Vec<usize>) {
        buf.clear();

        let faces = self.faces.value() as usize;
        for row in 0..faces {
            for i in (row..len).step_by(faces) {
                buf.push(i);
            }
        }
    }
}
impl<'l> KeySpace<'l> for Scytale<'l> {
    type Iter = ScytaleKeySpace<'l>;
    type Limit = ();

    fn key_space(lang: &'l Lang, _: ()) -> Self::Iter {
        ScytaleKeySpace {
            curr: Some(Scytale::default(lang)),
        }
    }
}

/// An iterator over the key space of the Scytale cipher.
pub struct ScytaleKeySpace<'l> {
    curr: Option<Scytale<'l>>,
}

impl<'l> Iterator for ScytaleKeySpace<'l> {
    type Item = Scytale<'l>;

    fn next(&mut self) -> Option<Self::Item> {
        let curr = self.curr.take()?;
        self.curr = curr.faces.successor(curr.alph).map(|faces| Scytale {
            alph: curr.alph,
            faces,
        });

        Some(curr)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{common::LANG, key::KeyError, prelude::BruteForce};

    #[test]
    fn new() {
        assert!(matches!(
            Scytale::new(&LANG, 1),
            Err(KeyError::OutOfRange {
                num: 1,
                min: 2,
                max: 100,
            })
        ));
        assert!(matches!(
            Scytale::new(&LANG, 101),
            Err(KeyError::OutOfRange {
                num: 101,
                min: 2,
                max: 100,
            })
        ));
        assert!(Scytale::new(&LANG, 100).is_ok());
        assert!(Scytale::new(&LANG, 2).is_ok());
    }

    #[test]
    fn encrypt_and_decrypt() {
        let cipher = Scytale::new(&LANG, 4).unwrap();

        let plaintext =
            "Why, sometimes I've believed as many as six impossible things before breakfast.";
        let ciphertext = &cipher.encrypt(plaintext);

        assert_eq!(
            ciphertext,
            "Wlx, shiibyeme S'vp foeoomds re aset si bim brmaleenea sytkia hfvsia esnsbigte.",
        );
        assert_eq!(&cipher.decrypt(ciphertext), plaintext);
    }

    #[test]
    fn solve() {
        let ciphertext =
            "Wlx, shiibyeme S'vp foeoomds re aset si bim brmaleenea sytkia hfvsia esnsbigte.";
        let Scytale { faces, .. } = Scytale::brute_force(&LANG, ciphertext, Default::default());

        assert_eq!(faces.value(), 4);
    }
}
