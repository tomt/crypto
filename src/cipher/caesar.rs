//! See the documentation for [`Caesar`].

use std::iter;

use crate::{lang::context::Inverse, prelude::*, solve::brute_force::KeySpace};

/// The caesar cipher is a monoalphabetic substitution cipher in
/// which letters are shifted by a constant number of places in
/// the alphabet. If the letters reach the end of the alphabet,
/// the shift wraps back to the beggining. This cipher has as many
/// keys as there are letters in the alphabet, so can simply be
/// brute forced. Decryption involves subtracting the shift from
/// the letter, so the cipher is asymmetrical.
#[derive(Debug, Clone, Copy)]
pub struct Caesar {
    pub shift: i32,
}

impl Cipher for Caesar {
    type Error = ();

    fn encrypt(&self, ctx: &Context, msg: Msg) -> Result<Msg, Self::Error> {
        Ok(msg.add(iter::repeat(self.shift)).modulo(ctx.alphabet_len))
    }

    fn decrypt(&self, ctx: &Context, msg: Msg) -> Result<Msg, Self::Error> {
        Ok(msg
            .subtract(iter::repeat(self.shift))
            .modulo(ctx.alphabet_len))
    }
}

impl Inverse for Caesar {
    fn inverse(self, ctx: &Context) -> Self {
        Caesar {
            shift: modulo(-self.shift, ctx.alphabet_len as i32),
        }
    }
}

impl Identity for Caesar {
    fn identity(_: &Context) -> Self {
        Caesar { shift: 0 }
    }
}

impl KeySpace for Caesar {
    type Iter = CaesarKeySpace;
    type Limit = ();

    fn key_space(ctx: &Context, _: Self::Limit) -> Self::Iter {
        CaesarKeySpace {
            alphabet_len: ctx.alphabet_len as i32,
            shift: 0,
        }
    }
}

// impl<'cribs> CribMatches<'cribs> for Caesar {
//     fn reconstruct_key(
//         lang: &'cribs Lang,
//         _: usize,
//         crib: &[i32],
//         ciphertext: &[i32],
//     ) -> Option<Self> {
//         let alph = Self::alph(lang);
//         let len = alph.alphabet_len();
//         let shift = util::modulo(ciphertext[0] - crib[0], len);

//         crib[1..]
//             .iter()
//             .zip(&ciphertext[1..])
//             .all(|(&p, &c)| util::modulo(c - p, len) == shift)
//             .then(|| Caesar::new(lang, shift).unwrap())
//     }
// }

/// An iterator over the key space of the Caesar cipher.
pub struct CaesarKeySpace {
    alphabet_len: i32,
    shift: i32,
}

impl Iterator for CaesarKeySpace {
    type Item = Caesar;

    fn next(&mut self) -> Option<Self::Item> {
        if self.shift == self.alphabet_len {
            None
        } else {
            let shift = self.shift;
            self.shift += 1;
            Some(Caesar { shift })
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::common::*;
    use crate::solve::brute_force::BruteForce;

    #[test]
    fn encrypt_and_decrypt() {
        let cipher = Caesar { shift: 13 };

        let plaintext =
            "Why, sometimes I've believed as many as six impossible things before breakfast.";
        let ciphertext =
            "Jul, fbzrgvzrf V'ir oryvrirq nf znal nf fvk vzcbffvoyr guvatf orsber oernxsnfg.";

        test_encrypt_decrypt(&cipher, plaintext, ciphertext);
    }

    #[test]
    fn brute_force() {
        let ciphertext =
            "Jul, fbzrgvzrf V'ir oryvrirq nf znal nf fvk vzcbffvoyr guvatf orsber oernxsnfg.";

        let (_, ciphertext) = TEXT_MAP.letters(ciphertext).split();

        let Caesar { shift, .. } =
            Caesar::brute_force(CONTEXT, &LANG, ciphertext, Default::default());

        assert_eq!(shift, 13);
    }

    // #[test]
    // fn crib() {
    //     let ciphertext =
    //         "Jul, fbzrgvzrf V'ir oryvrirq nf znal nf fvk vzcbffvoyr guvatf orsber oernxsnfg.";

    //     let cribs = Cribs::default().crib("before", None, Priority::Certain);
    //     let Caesar { shift, .. } =
    //         Caesar::crib_attack(&LANG, ciphertext, cribs, Default::default());
    //     assert_eq!(shift.cp(), 13);
    // }
}
