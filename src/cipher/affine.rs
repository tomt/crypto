//! See the documentation for [`Affine`].

use crate::{prelude::*, solve::brute_force::KeySpace};

#[derive(Debug, Clone, Copy)]
pub enum Error {
    NotCoprime,
}

/// Affine is a monoalphabetic substitution cipher, where each
/// letter is first converted to a number, then a function is
/// applied to that number: `f(n) = an + b (mod 26)` where `a` and `b`
/// are constants. The resulting number is then converted back into
/// a letter. In order for the inverse function to be one-to-one,
/// `a` must be [`Coprime`] to the alphabet length. Due to the small
/// number of values that `a` and `b` can take, the cipher can simply
/// be brute-forced. This cipher can take any alphabet length.
#[derive(Debug, Clone, Copy)]
pub struct Affine {
    pub a: i32,
    pub b: i32,
}

impl Affine {
    fn check(&self, ctx: &Context) -> Result<(), Error> {
        match util::gcd(self.a, ctx.alphabet_len as i32) {
            1 => Ok(()),
            _ => Err(Error::NotCoprime),
        }
    }
}

impl Cipher for Affine {
    type Error = Error;

    fn encrypt(&self, ctx: &Context, mut msg: Msg) -> Result<Msg, Self::Error> {
        self.check(ctx)?;

        msg = msg.sub(|cp| self.a * cp + self.b);
        msg = msg.modulo(ctx.alphabet_len);

        Ok(msg)
    }

    fn decrypt(&self, ctx: &Context, mut msg: Msg) -> Result<Msg, Self::Error> {
        self.check(ctx)?;

        let len = ctx.alphabet_len as i32;
        let inv = util::mmi(self.a, len).expect("a is coprime to alphabet_len");

        msg = msg.sub(|cp| inv * (cp - self.b));
        msg = msg.modulo(ctx.alphabet_len);

        Ok(msg)
    }
}

impl Identity for Affine {
    fn identity(_: &Context) -> Self {
        Affine { a: 1, b: 0 }
    }
}

impl KeySpace for Affine {
    type Iter = AffineKeySpace;
    type Limit = ();

    fn key_space(ctx: &Context, _: Self::Limit) -> Self::Iter {
        AffineKeySpace {
            alphabet_len: ctx.alphabet_len as i32,
            curr: Some(Affine::identity(ctx)),
        }
    }
}

// impl<'l> CribMatches<'l> for Affine<'l> {
//     fn reconstruct_key(lang: &'l Lang, _: usize, crib: &[i32], ciphertext: &[i32]) -> Option<Self> {
//         let alph = Self::alph(lang);
//         util::solve_linear_modular(crib, ciphertext, alph.alphabet_len())
//             .map(|(a, b)| Affine::new(lang, a, b).unwrap())
//     }
// }

/// An iterator over the key space of the Affine cipher.
pub struct AffineKeySpace {
    alphabet_len: i32,
    curr: Option<Affine>,
}

impl Iterator for AffineKeySpace {
    type Item = Affine;

    fn next(&mut self) -> Option<Self::Item> {
        let len = self.alphabet_len;
        let Affine { mut a, mut b } = self.curr.take()?;

        if b + 1 < len {
            b += 1;
            self.curr = Some(Affine { a, b });
        } else {
            b = 0;
            a += 1;

            while a < len && util::gcd(a, len) != 1 {
                a += 1;
            }

            self.curr = match a == len {
                true => None,
                false => Some(Affine { a, b }),
            }
        }

        self.curr
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{common::*, solve::brute_force::BruteForce};

    #[test]
    fn encrypt_and_decrypt() {
        let cipher = Affine { a: 19, b: 8 };

        let plaintext =
            "Why, sometimes I've believed as many as six impossible things before breakfast.";
        let ciphertext =
            "Klw, mocgfecgm E'rg bgjegrgn im civw im med echommebjg flevsm bgzotg btgiqzimf.";

        test_encrypt_decrypt(&cipher, plaintext, ciphertext);
    }

    #[test]
    fn brute_force() {
        let ciphertext =
            "Klw, mocgfecgm E'rg bgjegrgn im civw im med echommebjg flevsm bgzotg btgiqzimf.";

        let (_, ciphertext) = TEXT_MAP.letters(ciphertext).split();

        let Affine { a, b } = Affine::brute_force(CONTEXT, &LANG, ciphertext, Default::default());

        assert_eq!(a, 19);
        assert_eq!(b, 8);
    }

    // #[test]
    // fn crib() {
    //     let ciphertext =
    //         "Klw, mocgfecgm E'rg bgjegrgn im civw im med echommebjg flevsm bgzotg btgiqzimf.";

    //     let cribs = Cribs::default().crib("impossible", None, Priority::Certain);
    //     let Affine { a, b, .. } = Affine::crib_attack(&LANG, ciphertext, cribs, Default::default());

    //     assert_eq!(a.num(), 19);
    //     assert_eq!(b.cp(), 8);
    // }
}
