//! See the documentation for [`Quagmire4`]

use crate::{
    analysis::stream,
    cipher::Cipher,
    key::{FillStrategy, Key, KeyFrom, KeyResult, Keyword, SubAlph},
    lang::{
        with_alphabet::{StatsSize, WithAlphabet},
        AlphabetLen, Lang,
    },
    prelude::DictionaryAttack,
    util,
};
use rayon::prelude::*;

/// Quagmire 4 uses distinct keyed alphabet for the plaintext (input)
/// alphabet and the ciphertext alphabets. The ciphertext alphabets are
/// shifted so that the first character in each period is the corresponding
/// key character of the third keyword.
#[derive(Debug)]
pub struct Quagmire4<'l> {
    alph: WithAlphabet<'l>,
    plain_sub: SubAlph<'l>,
    cipher_sub: SubAlph<'l>,
    keyword: Keyword<'l>,
}

impl<'l> Quagmire4<'l> {
    /// Creates a new [`Quagmire4`] cipher from the plaintext alphabet,
    /// ciphertext alphabet and shift keyword.
    pub fn new<P, C, K>(
        lang: &'l Lang,
        plain_alph: P,
        cipher_alph: C,
        keyword: K,
    ) -> KeyResult<Self>
    where
        SubAlph<'l>: KeyFrom<'l, P>,
        SubAlph<'l>: KeyFrom<'l, C>,
        Keyword<'l>: KeyFrom<'l, K>,
    {
        Ok(Self {
            alph: Self::alph(lang),
            plain_sub: SubAlph::key_from(Self::alph(lang), plain_alph)?,
            cipher_sub: SubAlph::key_from(Self::alph(lang), cipher_alph)?,
            keyword: Keyword::key_from(Self::alph(lang), keyword)?,
        })
    }
    /// Gets the plaintext substitution alphabet.
    pub fn plaintext_alph(&self) -> &SubAlph {
        &self.plain_sub
    }
    /// Gets the ciphertext substitution alphabet.
    pub fn ciphertext_alph(&self) -> &SubAlph {
        &self.cipher_sub
    }
    /// Gets the keyword for the ciphertext alphabets.
    pub fn keyword(&self) -> &Keyword {
        &self.keyword
    }
}
impl<'l> Cipher<'l> for Quagmire4<'l> {
    fn alphabet_len() -> AlphabetLen {
        AlphabetLen::Any
    }
    fn random(lang: &'l Lang) -> Self {
        Self {
            alph: Self::alph(lang),
            plain_sub: SubAlph::random(Self::alph(lang)),
            cipher_sub: SubAlph::random(Self::alph(lang)),
            keyword: Keyword::random(Self::alph(lang)),
        }
    }
    fn default(lang: &'l Lang) -> Self {
        Self {
            alph: Self::alph(lang),
            plain_sub: SubAlph::default(Self::alph(lang)),
            cipher_sub: SubAlph::default(Self::alph(lang)),
            keyword: Keyword::default(Self::alph(lang)),
        }
    }
    fn encrypt(&self, msg: &str) -> String {
        stream::with(msg, self.alph, &self.keyword, |cp, key| {
            self.cipher_sub.encrypt(util::modulo(
                self.plain_sub.decrypt(cp) + self.cipher_sub.decrypt(key),
                self.alph.alphabet_len(),
            ))
        })
    }
    fn decrypt(&self, msg: &str) -> String {
        stream::with(msg, self.alph, &self.keyword, |cp, key| {
            self.plain_sub.encrypt(util::modulo(
                self.cipher_sub.decrypt(cp) - self.cipher_sub.decrypt(key),
                self.alph.alphabet_len(),
            ))
        })
    }
}
impl<'l, 'kw> DictionaryAttack<'l, 'kw> for Quagmire4<'l> {
    type Options = Quagmire4DictionaryOptions;

    fn dictionary_attack(
        lang: &'l Lang,
        msg: &str,
        keywords: impl Iterator<Item = &'kw str> + Send,
        options: Self::Options,
    ) -> Self {
        // find all keywords of required length.
        let alph = Self::alph(lang);
        let len = alph.alphabet_len();
        let kw: Vec<Vec<i32>> = keywords
            .map(|kw| alph.code_points(kw).collect::<Vec<i32>>())
            .filter(|kw| (options.min_keyword_len..=options.max_keyword_len).contains(&kw.len()))
            .take(options.max_keywords)
            .collect();
        let ct: Vec<i32> = alph.code_points(msg).collect();

        let best_key = kw
            .par_iter()
            .map(|keyword| {
                println!("{keyword:?}");

                let mut best_score = f32::MIN;
                let mut best_key = None;

                for cipher_fill in FillStrategy::iter() {
                    for cipher_kw in &kw {
                        let cipher = cipher_fill.fill(cipher_kw, len as usize);
                        let cipher_inv = util::invert(&cipher);

                        for plain_fill in FillStrategy::iter() {
                            for plain_kw in &kw {
                                let plain = plain_fill.fill(plain_kw, len as usize);

                                let score = alph.score(
                                    ct.iter().zip(keyword.iter().cycle()).map(|(&cp, &key)| {
                                        plain[util::modulo(
                                            cipher_inv[cp as usize] - cipher_inv[key as usize],
                                            len,
                                        ) as usize]
                                    }),
                                    options.stats_size,
                                );

                                if score > best_score {
                                    best_score = score;
                                    best_key = Some((
                                        (plain_kw, plain_fill),
                                        (cipher_kw, cipher_fill),
                                        keyword,
                                    ));
                                }
                            }
                        }
                    }
                }

                (best_score, best_key)
            })
            .max_by(|(s1, _), (s2, _)| s1.partial_cmp(s2).unwrap())
            .and_then(|(_, x)| x)
            .map(|((k1, f1), (k2, f2), k3)| {
                (f1.fill(k1, len as usize), f2.fill(k2, len as usize), k3)
            });

        best_key
            .and_then(|(plain, cipher, keyword)| {
                Self::new(lang, &plain[..], &cipher[..], &keyword[..]).ok()
            })
            .unwrap_or_else(|| Self::default(lang))
    }
}

/// Options for configuring the Quagmire4 dictionary attack.
pub struct Quagmire4DictionaryOptions {
    /// Minimum keyword length, filters the provided iterator.
    pub min_keyword_len: usize,
    /// Maximum keyword length, filters the provided iterator.
    pub max_keyword_len: usize,
    /// Maximum number of keywords to try, the algorithm is quadratic so
    /// this number has a significant effect.
    pub max_keywords: usize,
    /// StatsSize for scoring decryptions.
    pub stats_size: StatsSize,
}

impl Default for Quagmire4DictionaryOptions {
    fn default() -> Self {
        Self {
            min_keyword_len: 2,
            max_keyword_len: 10,
            max_keywords: 225,
            stats_size: StatsSize::Quadgrams,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::common::LANG;

    #[test]
    fn encrypt_and_decrypt() {
        let cipher = Quagmire4::new(&LANG, "quagmire", "cipher", "keyword").unwrap();

        let plaintext =
            "Why, sometimes I've believed as many as six impossible things before breakfast.";
        let ciphertext = &cipher.encrypt(plaintext);

        assert_eq!(
            ciphertext,
            "Zzt, cyvnxvdfa W'ui ijmhyuih ac pthb sp chq lojccawzsx hjhgjw ijgyxn fwjyvzgwl."
        );
        assert_eq!(&cipher.decrypt(ciphertext), plaintext)
    }
}
