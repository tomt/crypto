//! See the documentation for [`Beaufort`].

use crate::{
    analysis::{
        brute_force::{KeySpace, MaxPeriod},
        crib_attack::CribMatches,
        dictionary_attack::FromKeyword,
        periodic, stream, Solve,
    },
    cipher::Cipher,
    key::{Key, KeyFrom, KeyResult, Keyword, Successor},
    lang::{
        with_alphabet::{StatsSize, WithAlphabet},
        AlphabetLen, Lang,
    },
    util,
};

/// The [`Options`](Solve::Options) type for [`Beaufort`]. Used to
/// configure [`Solve::solve`].
pub struct BeaufortSolve {
    /// The [`StatsSize`] that should be used for scoring.
    pub stats_size: StatsSize,
    /// The maximum keyword length that should be tested.
    pub max_key_length: usize,
}
impl Default for BeaufortSolve {
    fn default() -> Self {
        Self {
            stats_size: StatsSize::default(),
            max_key_length: 30,
        }
    }
}

/// Beaufort is similar to the classic vigenere cipher, but uses the
/// functions:
///     encrypt (cipher_ch, key_ch) = key_ch - cipher_ch (mod m)
///     decrypt ( plain_ch, key_ch) = key_ch -  plain_ch (mod m)
/// and hence is a symmetrical cipher.
#[derive(Debug)]
pub struct Beaufort<'l> {
    alph: WithAlphabet<'l>,
    keyword: Keyword<'l>,
}

impl<'l> Beaufort<'l> {
    /// Creates a new [`Beaufort`] cipher with the `keyword`
    /// provided.
    pub fn new<K>(lang: &'l Lang, keyword: K) -> KeyResult<Self>
    where
        Keyword<'l>: KeyFrom<'l, K>,
    {
        Ok(Self {
            alph: Self::alph(lang),
            keyword: Keyword::key_from(Self::alph(lang), keyword)?,
        })
    }
    /// Gets the keyword.
    pub fn keyword(&self) -> &Keyword<'l> {
        &self.keyword
    }
}
impl<'l> Cipher<'l> for Beaufort<'l> {
    fn alphabet_len() -> AlphabetLen {
        AlphabetLen::Any
    }
    fn random(lang: &'l Lang) -> Self {
        Self {
            alph: Self::alph(lang),
            keyword: Keyword::random(Self::alph(lang)),
        }
    }
    fn default(lang: &'l Lang) -> Self {
        Self {
            alph: Self::alph(lang),
            keyword: Keyword::default(Self::alph(lang)),
        }
    }
    fn encrypt(&self, msg: &str) -> String {
        stream::with(msg, self.alph, self.keyword(), |cp, k| k - cp)
    }
    fn decrypt(&self, msg: &str) -> String {
        self.encrypt(msg)
    }
}
impl<'l> Solve<'l> for Beaufort<'l> {
    type Options = BeaufortSolve;

    fn solve(lang: &'l Lang, msg: &str, options: Self::Options) -> Self {
        let alph = Self::alph(lang);
        let len = alph.alphabet_len();
        let ct: Vec<i32> = alph.code_points(msg).collect();
        let (_, kw) = periodic::solve_vig(
            &ct,
            alph,
            options.stats_size,
            |key_cp, cipher_cp, _| util::modulo(key_cp - cipher_cp, len),
            1,
            options.max_key_length,
        );

        Self {
            alph,
            keyword: Keyword::key_from(alph, &kw[..]).unwrap(),
        }
    }
}
impl<'l> KeySpace<'l> for Beaufort<'l> {
    type Iter = BeaufortKeySpace<'l>;
    type Limit = MaxPeriod;

    fn key_space(lang: &'l Lang, MaxPeriod(max_len): Self::Limit) -> Self::Iter {
        BeaufortKeySpace {
            max_len,
            curr: Some(Self::default(lang)),
        }
    }
}
impl<'l> CribMatches<'l> for Beaufort<'l> {
    fn reconstruct_key(
        lang: &'l Lang,
        pos: usize,
        crib: &[i32],
        ciphertext: &[i32],
    ) -> Option<Self> {
        let alph = Self::alph(lang);
        let shifts: Vec<i32> = crib
            .iter()
            .zip(ciphertext)
            .map(|(&a, &b)| util::modulo(a + b, alph.alphabet_len()))
            .collect();

        'outer: for period in 3..(crib.len() + 1) / 2 {
            for col in 0..period {
                for i in (period + col..crib.len()).step_by(period) {
                    if shifts[i] != shifts[col] {
                        continue 'outer;
                    }
                }
            }

            let mut key = vec![0; period];

            for i in 0..period {
                key[(i + pos) % period] = shifts[i];
            }

            return Some(Beaufort::new(lang, &key[..]).unwrap());
        }

        None
    }
}
impl<'l, 'kw> FromKeyword<'l, 'kw> for Beaufort<'l> {
    type Iter = std::iter::Once<Beaufort<'l>>;
    type Variants = ();

    fn from_keyword(lang: &'l Lang, keyword: &'kw str, _: Self::Variants) -> Self::Iter {
        std::iter::once(Self::new(lang, keyword).unwrap())
    }
}

/// An iterator over the key space of [`Beaufort`].
pub struct BeaufortKeySpace<'l> {
    max_len: usize,
    curr: Option<Beaufort<'l>>,
}

impl<'l> Iterator for BeaufortKeySpace<'l> {
    type Item = Beaufort<'l>;

    fn next(&mut self) -> Option<Self::Item> {
        let curr = self.curr.take()?;
        self.curr = curr
            .keyword
            .clone()
            .successor(curr.alph)
            .filter(|kw| kw.len() <= self.max_len)
            .map(|keyword| Beaufort {
                alph: curr.alph,
                keyword,
            });

        Some(curr)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::common::LANG;

    #[test]
    fn encrypt_and_decrypt() {
        let cipher = Beaufort::new(&LANG, "beaufort").unwrap();

        let plaintext =
            "Why, sometimes I've believed as many as six impossible things before breakfast.";
        let ciphertext = &cipher.encrypt(plaintext);

        assert_eq!(
            ciphertext,
            "Fxc, crcnatswc X'tn sxtsqkko tj sahh oz bth siqazbtdpq mhjgvm zqaaap anwuvjrbi."
        );
        assert_eq!(plaintext, cipher.decrypt(ciphertext));
    }

    #[test]
    fn solve() {
        let key = [10, 4, 24];

        let cipher = Beaufort::new(&LANG, &key[..]).unwrap();

        let plaintext =
            "Why, sometimes I've believed as many as six impossible things before breakfast.";
        let ciphertext = cipher.encrypt(plaintext);

        let Beaufort { keyword, .. } = Beaufort::solve(&LANG, &ciphertext, Default::default());

        assert_eq!(keyword.word(), key);
    }
}
