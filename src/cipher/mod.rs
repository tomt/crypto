//! Provides cipher algorithms which implement the [`Cipher`] trait.

use crate::lang::{
    context::{Context, Inverse},
    msg::Msg,
};

/// A cipher algorithm can encrypt and decrypt messages.
pub trait Cipher {
    type Error: std::fmt::Debug;

    fn encrypt(&self, ctx: &Context, msg: Msg) -> Result<Msg, Self::Error>;
    fn decrypt(&self, ctx: &Context, msg: Msg) -> Result<Msg, Self::Error>;
}

pub trait Symmetric {
    type Error: std::fmt::Debug;

    fn run(&self, ctx: &Context, msg: Msg) -> Result<Msg, Self::Error>;
}

impl<C> Inverse for C
where
    C: Symmetric,
{
    fn inverse(self, _: &Context) -> Self {
        self
    }
}

impl<C> Cipher for C
where
    C: Symmetric,
{
    type Error = <C as Symmetric>::Error;

    fn encrypt(&self, ctx: &Context, msg: Msg) -> Result<Msg, Self::Error> {
        self.run(ctx, msg)
    }

    fn decrypt(&self, ctx: &Context, msg: Msg) -> Result<Msg, Self::Error> {
        self.run(ctx, msg)
    }
}

mod affine;
mod atbash;
// mod beaufort;
mod bellaso;
// mod block_transposition;
mod caesar;
mod classic_vigenere;
// mod periodic_affine;
// mod quagmire1;
// mod quagmire2;
// mod quagmire3;
// mod quagmire4;
// mod railfence;
// mod scytale;
mod substitution;

pub use affine::Affine;
pub use atbash::Atbash;
// pub use beaufort::{Beaufort, BeaufortSolve};
pub use bellaso::{Bellaso, BellasoSolve};
// pub use block_transposition::BlockTransposition;
pub use caesar::Caesar;
pub use classic_vigenere::{ClassicVigenere, ClassicVigenereSolve};
// pub use periodic_affine::{PeriodicAffine, PeriodicAffineSolve};
// pub use quagmire1::{Quagmire1, Quagmire1Solve};
// pub use quagmire2::Quagmire2;
// pub use quagmire3::Quagmire3;
// pub use quagmire4::Quagmire4;
// pub use railfence::Railfence;
// pub use scytale::Scytale;
pub use substitution::{Substitution, SubstitutionSolve};
