//! See the documentation for [`Atbash`]

use crate::prelude::*;

/// Atbash is a keyless monoalphabetic substitution cipher in which
/// letters at one end of the alphabet are substituted for those at
/// the other end: `A` -> `Z`, `B` -> `Y`. Any alphabet length can
/// be used with this cipher.
pub struct Atbash;

impl Symmetric for Atbash {
    type Error = ();

    fn run(&self, ctx: &Context, msg: Msg) -> Result<Msg, Self::Error> {
        Ok(msg.sub(|cp| -(cp + 1)).modulo(ctx.alphabet_len))
    }
}

impl Identity for Atbash {
    fn identity(_: &Context) -> Self {
        Self
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::common::test_encrypt_decrypt;

    #[test]
    fn encrypt_and_decrypt() {
        let cipher = Atbash;

        let plaintext =
            "Why, sometimes I've believed as many as six impossible things before breakfast.";
        let ciphertext =
            "Dsb, hlnvgrnvh R'ev yvorvevw zh nzmb zh hrc rnklhhryov gsrmth yvuliv yivzpuzhg.";

        test_encrypt_decrypt(&cipher, plaintext, ciphertext);
    }
}
