//! See the documentation for [`Quagmire3`]

use crate::{
    analysis::stream,
    cipher::Cipher,
    key::{Key, KeyFrom, KeyResult, Keyword, SubAlph},
    lang::{
        with_alphabet::{StatsSize, WithAlphabet},
        AlphabetLen, Lang,
    },
    prelude::DictionaryAttack,
    util,
};
use rayon::prelude::*;

/// Quagmire 3 uses the same keyed alphabet for the plaintext (input)
/// alphabet and the ciphertext alphabets. The ciphertext alphabets are
/// shifted so that the first character in each period is the corresponding
/// key character.
#[derive(Debug)]
pub struct Quagmire3<'l> {
    alph: WithAlphabet<'l>,
    sub: SubAlph<'l>,
    keyword: Keyword<'l>,
}

impl<'l> Quagmire3<'l> {
    /// Creates a new [`Quagmire3`] cipher from the substitution alphabet
    /// and the shift keyword.
    pub fn new<A, K>(lang: &'l Lang, sub: A, keyword: K) -> KeyResult<Self>
    where
        SubAlph<'l>: KeyFrom<'l, A>,
        Keyword<'l>: KeyFrom<'l, K>,
    {
        Ok(Self {
            alph: Self::alph(lang),
            sub: SubAlph::key_from(Self::alph(lang), sub)?,
            keyword: Keyword::key_from(Self::alph(lang), keyword)?,
        })
    }
    /// Gets the substitution alphabet.
    pub fn sub(&self) -> &SubAlph {
        &self.sub
    }
    /// Gets the keyword for the ciphertext alphabets.
    pub fn keyword(&self) -> &Keyword {
        &self.keyword
    }
}
impl<'l> Cipher<'l> for Quagmire3<'l> {
    fn alphabet_len() -> AlphabetLen {
        AlphabetLen::Any
    }
    fn random(lang: &'l Lang) -> Self {
        Self {
            alph: Self::alph(lang),
            sub: SubAlph::random(Self::alph(lang)),
            keyword: Keyword::random(Self::alph(lang)),
        }
    }
    fn default(lang: &'l Lang) -> Self {
        Self {
            alph: Self::alph(lang),
            sub: SubAlph::default(Self::alph(lang)),
            keyword: Keyword::default(Self::alph(lang)),
        }
    }
    fn encrypt(&self, msg: &str) -> String {
        stream::with(msg, self.alph, &self.keyword, |cp, key| {
            self.sub.encrypt(util::modulo(
                self.sub.decrypt(key) + self.sub.decrypt(cp),
                self.alph.alphabet_len(),
            ))
        })
    }
    fn decrypt(&self, msg: &str) -> String {
        stream::with(msg, self.alph, &self.keyword, |cp, key| {
            self.sub.encrypt(util::modulo(
                self.sub.decrypt(cp) - self.sub.decrypt(key),
                self.alph.alphabet_len(),
            ))
        })
    }
}
impl<'l, 'kw> DictionaryAttack<'l, 'kw> for Quagmire3<'l> {
    type Options = Quagmire3DictionaryOptions;

    fn dictionary_attack(
        lang: &'l Lang,
        msg: &str,
        keywords: impl Iterator<Item = &'kw str> + Send,
        options: Self::Options,
    ) -> Self {
        // find all keywords of required length.
        let alph = Self::alph(lang);
        let len = alph.alphabet_len();
        let kw: Vec<Vec<i32>> = keywords
            .map(|kw| alph.code_points(kw).collect::<Vec<i32>>())
            .filter(|kw| (options.min_keyword_len..=options.max_keyword_len).contains(&kw.len()))
            .take(options.max_keywords)
            .collect();
        let ct: Vec<i32> = alph.code_points(msg).collect();

        let best_key = kw
            .par_iter()
            .map(|sub_kw| {
                let mut best_score = f32::MIN;
                let mut best_key = None;
                let mut inv = vec![0; len as usize];

                let mut test_alphabet = |sub: Vec<i32>| {
                    util::invert_into(&sub, &mut inv);

                    for keyword in kw.iter() {
                        let score = alph.score(
                            ct.iter().zip(keyword.iter().cycle()).map(|(&cp, &key)| {
                                sub[util::modulo(inv[cp as usize] - inv[key as usize], len)
                                    as usize]
                            }),
                            options.stats_size,
                        );

                        if score > best_score {
                            best_score = score;
                            best_key = Some((sub.clone(), keyword));
                        }
                    }
                };

                test_alphabet(util::fill_continue(sub_kw, len as usize));
                test_alphabet(util::fill_continue_max(sub_kw, len as usize));
                test_alphabet(util::fill_start(sub_kw, len as usize));

                (best_score, best_key)
            })
            .max_by(|(s1, _), (s2, _)| s1.partial_cmp(s2).unwrap())
            .and_then(|(_, x)| x);

        best_key
            .and_then(|(cipher_alph_kw, cipher_kw)| {
                Self::new(lang, &cipher_alph_kw[..], &cipher_kw[..]).ok()
            })
            .unwrap_or_else(|| Self::default(lang))
    }
}

/// Options for configuring the Quagmire3 dictionary attack.
pub struct Quagmire3DictionaryOptions {
    /// Minimum keyword length, filters the provided iterator.
    pub min_keyword_len: usize,
    /// Maximum keyword length, filters the provided iterator.
    pub max_keyword_len: usize,
    /// Maximum number of keywords to try, the algorithm is quadratic so
    /// this number has a significant effect.
    pub max_keywords: usize,
    /// StatsSize for scoring decryptions.
    pub stats_size: StatsSize,
}

impl Default for Quagmire3DictionaryOptions {
    fn default() -> Self {
        Self {
            min_keyword_len: 3,
            max_keyword_len: 5,
            max_keywords: 2_000,
            stats_size: StatsSize::Quadgrams,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::common::LANG;

    #[test]
    fn encrypt_and_decrypt() {
        let cipher = Quagmire3::new(&LANG, "quagmire", "keyword").unwrap();

        let plaintext =
            "Why, sometimes I've believed as many as six impossible things before breakfast.";
        let ciphertext = &cipher.encrypt(plaintext);

        assert_eq!(
            ciphertext,
            "Mss, hajraldqm K'tv maiwntvr bh vflr hk hwq mpzhhmkzbo lawwau maualr fnaydouuc.",
        );
        assert_eq!(&cipher.decrypt(ciphertext), plaintext)
    }
}
