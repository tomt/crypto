//! Handling the conversion from an input string to a lossless abstract representation
//! (a vec of `Letter`s) & back again.

use crate::lang::letter::{Case, Letter};
use std::{collections::HashMap, iter};

use super::{letter::Template, msg::Msg};

#[derive(Debug, Clone, Copy)]
pub enum Error {
    BadTemplate,
}

#[derive(Debug, Clone)]
pub struct Letters {
    alphabet_len: usize,
    data: Vec<Letter>,
}

impl Letters {
    pub fn split(self) -> (Vec<Template>, Msg) {
        let alphabet_len = self.alphabet_len;
        let mut templates = Vec::with_capacity(self.data.len());
        let mut code_points = vec![];

        for letter in self {
            templates.push(Template::from(letter));

            if let Some(cp) = letter.cp() {
                code_points.push(cp);
            }
        }

        let msg = Msg {
            data: code_points,
            alphabet_len,
        };

        (templates, msg)
    }

    pub fn join(templates: Vec<Template>, msg: Msg) -> Result<Self, Error> {
        let alphabet_len = msg.alphabet_len;
        let mut letters = Vec::with_capacity(templates.len());
        let mut code_points = msg.into_iter();

        for template in templates {
            letters.push(match template {
                Template::Punct(punct) => Letter::Punct(punct),
                Template::Cp(case) => match code_points.next() {
                    Some(cp) => Letter::Cp(case, cp),
                    None => return Err(Error::BadTemplate),
                },
            });
        }

        Ok(Self {
            data: letters,
            alphabet_len,
        })
    }

    pub fn case(msg: Msg, case: Case) -> Self {
        Letters {
            alphabet_len: msg.alphabet_len,
            data: msg
                .data
                .into_iter()
                .map(|cp| Letter::Cp(case, cp))
                .collect(),
        }
    }

    pub fn upper(msg: Msg) -> Self {
        Self::case(msg, Case::Upper)
    }

    pub fn lower(msg: Msg) -> Self {
        Self::case(msg, Case::Lower)
    }
}

impl IntoIterator for Letters {
    type Item = Letter;
    type IntoIter = std::vec::IntoIter<Letter>;

    fn into_iter(self) -> Self::IntoIter {
        self.data.into_iter()
    }
}

/// Stores the mapping from input text to/from abstract representation.
#[derive(Debug, Clone)]
pub struct TextMap {
    // Only for Letter::Cp
    map: HashMap<char, Letter>,
    inv: HashMap<i32, (char, char)>,
}

impl TextMap {
    /// Attempts to determine a mapping from the input.
    pub fn infer(_text: &str) -> Self {
        // TODO: use frequency counts & regularities to determine which
        //       letters to include.
        // TODO: add options for quickly creating custom textmaps, eg. treating
        //       uppercase & lowercase letters differently.

        let mut map = HashMap::new();
        let mut inv = HashMap::new();

        const UPPER: &str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        const LOWER: &str = "abcdefghijklmnopqrstuvwxyz";

        for (i, (lower, upper)) in iter::zip(LOWER.chars(), UPPER.chars()).enumerate() {
            map.insert(lower, Letter::Cp(Case::Lower, i as i32));
            map.insert(upper, Letter::Cp(Case::Upper, i as i32));

            inv.insert(i as i32, (lower, upper));
        }

        Self { map, inv }
    }

    pub fn alphabet_len(&self) -> usize {
        self.inv.len()
    }

    pub fn letters(&self, text: &str) -> Letters {
        Letters {
            alphabet_len: self.alphabet_len(),
            data: self.letters_iter(text).collect(),
        }
    }

    pub fn letters_iter<'s, 'data: 's>(
        &'s self,
        text: &'data str,
    ) -> impl Iterator<Item = Letter> + 's {
        text.chars()
            .map(|c| self.map.get(&c).copied().unwrap_or(Letter::Punct(c)))
    }

    pub fn msg(&self, text: &str) -> Msg {
        Msg {
            alphabet_len: self.alphabet_len(),
            data: self.letters_iter(text).filter_map(Letter::cp).collect(),
        }
    }

    pub fn to_str(&self, text: impl IntoIterator<Item = Letter>) -> String {
        text.into_iter()
            .map(|letter| match letter {
                Letter::Punct(p) => p,
                Letter::Cp(case, c) => match (case, self.inv[&c]) {
                    (Case::Lower, (lower, _)) => lower,
                    (Case::Upper, (_, upper)) => upper,
                },
            })
            .collect()
    }
}
