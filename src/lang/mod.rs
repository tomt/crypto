//! Module for handling the conversion from input text to an abstract
//! representation (numbers between 0 and the alphabet length) and
//! storing stats needed to score a candidate plaintext.

use std::collections::HashMap;

use crate::{
    prelude::stats::ScoringIndex,
    util::stats::{self, StatsSize},
};
use nanoserde::{DeBin, SerBin};

pub mod context;
pub mod letter;
pub mod msg;
pub mod text_map;

/// Can be used to calculate various statistics over an input text by
/// comparison to those computed for a corpus.
#[derive(SerBin, DeBin, Default, Debug)]
pub struct Lang {
    stats: HashMap<StatsSize, Stats>,
}

impl Lang {
    /// Create a new `Lang` from a corpus.
    pub fn new<T, I>(corpus: I) -> Self
    where
        I: IntoIterator<Item = T> + Clone,
        T: ScoringIndex,
    {
        let mut stats = HashMap::new();

        for stats_size in StatsSize::iter() {
            stats.insert(stats_size, Stats::new(stats_size, corpus.clone()));
        }

        Self { stats }
    }

    pub fn stats(&self, stats_size: StatsSize) -> &Stats {
        &self.stats[&stats_size]
    }

    pub fn stats_slice(&self, stats_size: StatsSize) -> &[f32] {
        &self.stats(stats_size).stats
    }

    /// Scores a text using the angle between n-dimensional vectors,
    /// where n is the alphabet length. Always between -1 and 1, with 1 meaning
    /// the vectors are parallel as cos(0) = 1, cos(pi/2) = 0.
    pub fn score_vector<T, I>(&self, data: I, stats_size: StatsSize) -> f32
    where
        I: IntoIterator<Item = T>,
        T: ScoringIndex,
    {
        // Expected values calculated by doing exp(log prob) = prob.
        // All these probabilities are scaled by 1/N, where N is the
        // number of unigrams in the text, so angle is unaffected.
        let expected = self.stats(stats_size).frequencies_iter();
        let actual = stats::frequencies(&stats::counts(data, stats_size));
        stats::dp(expected, actual)
    }

    /// Calculates the chi-squared for an iterator over code points.
    pub fn chi_squared<T, I>(&self, data: I) -> f32
    where
        I: Iterator<Item = T>,
        T: ScoringIndex,
    {
        let counts = stats::counts(data, StatsSize::Unigrams);
        stats::chi_squared(
            stats::frequencies(&counts),
            self.stats(StatsSize::Unigrams).frequencies_iter(),
        )
    }

    /// Calculates the unigram, bigram, trigram or quadgram score of
    /// an iterator over codepoints.
    pub fn score<T, I>(&self, data: I, stats_size: StatsSize) -> f32
    where
        I: IntoIterator<Item = T>,
        T: ScoringIndex,
    {
        let stats = self.stats_slice(stats_size);
        let mut len = 0;
        let mut sum = 0.0;
        for idx in stats::scoring_indices(data, stats_size) {
            len += 1;
            sum += stats[idx];
        }

        // divide by number of letter groups
        let score = sum / len as f32;
        let stats = self.stats(stats_size);

        // normalise between 0 and 1
        let norm = (score - stats.min) / (stats.max - stats.min);
        let ax = stats.a * norm;

        ax * (2.0 - ax)
    }
}

#[derive(SerBin, DeBin, Default, Debug)]
pub struct Stats {
    stats: Vec<f32>,
    min: f32,
    a: f32,
    max: f32,
}

impl Stats {
    pub fn new<I, T>(stats_size: StatsSize, corpus: I) -> Self
    where
        I: IntoIterator<Item = T>,
        T: ScoringIndex,
    {
        let stats = stats::log_probs(&stats::counts(corpus, stats_size));

        let mut min = f32::MAX;
        let mut max = f32::MIN;
        let mut avg: f64 = 0.0;

        for &s in &stats {
            min = min.min(s);
            max = max.max(s);

            let s = s as f64;
            avg += s.exp() * s;
        }

        //   1 - ( 1 - ( ax ) ) ^ 2
        // = 1 - ( 1 - 2ax + aaxx )
        // = 1 -   1 + 2ax - aaxx
        // =           2ax - aaxx
        // =      ax ( 2   -  ax  )
        //
        // f(x) = ax ( 2 - ax )
        //
        // need f(avg) = 1
        // 1 = a (avg) ( 2 - a (avg) )
        // 1 = [2(avg)]a - [(avg)]a^2
        // [avg]a^2 - [2(avg)]a + 1 = 0

        // a = [2(avg) + sqrt(4(avg)^2 - 4(avg)(1))] / 2(avg)
        //   = 1 + sqrt ( avg * ( avg - 1 ) ) / avg

        let a = 1.0 - (avg * (avg - 1.0)).sqrt() / avg;
        let a = a as f32;

        Self { min, a, max, stats }
    }

    pub fn frequencies(&self) -> Vec<f32> {
        self.frequencies_iter().collect()
    }

    pub fn frequencies_iter(&self) -> impl Iterator<Item = f32> + '_ {
        self.stats.iter().map(|log_prob| log_prob.exp())
    }
}
