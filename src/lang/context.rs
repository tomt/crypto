#[derive(Debug, Clone, Copy)]
pub struct Context {
    pub alphabet_len: usize,
}

pub trait Identity {
    fn identity(ctx: &Context) -> Self;
}

pub trait Inverse {
    fn inverse(self, ctx: &Context) -> Self;
}
