//! Types for modelling letters: Code points or Punctuation.

use std::fmt::Debug;

/// Upper or lower case (for a codepoint).
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Case {
    /// Uppercase
    Upper,
    /// Lowercase
    Lower,
}

#[derive(Debug, Clone, Copy)]
pub enum Template {
    Cp(Case),
    Punct(char),
}

impl From<Letter> for Template {
    fn from(value: Letter) -> Self {
        match value {
            Letter::Cp(case, _) => Template::Cp(case),
            Letter::Punct(punct) => Template::Punct(punct),
        }
    }
}

/// A letter: either a codepoint or a punctuation character.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Letter {
    Cp(Case, i32),
    Punct(char),
}

impl Letter {
    /// Gets the optional codepoint.
    pub fn cp(self) -> Option<i32> {
        match self {
            Letter::Cp(_, cp) => Some(cp),
            _ => None,
        }
    }

    /// Gets the optional punctuation character.
    pub fn punct(self) -> Option<char> {
        match self {
            Letter::Punct(c) => Some(c),
            _ => None,
        }
    }

    /// Checks whether the contained letter is uppercase.
    pub fn is_upper(self) -> bool {
        match self {
            Letter::Cp(case, _) => case == Case::Upper,
            Letter::Punct(p) => p.is_uppercase(),
        }
    }

    /// Checks whether the contained letter is lowercase.
    pub fn is_lower(self) -> bool {
        match self {
            Letter::Cp(case, _) => case == Case::Lower,
            Letter::Punct(p) => p.is_lowercase(),
        }
    }

    /// Checks whether the letter is a codepoint.
    pub fn is_cp(self) -> bool {
        self.cp().is_some()
    }

    /// Checks whether the letter is punctuation.
    pub fn is_punct(self) -> bool {
        self.punct().is_some()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn punctuation() {
        let letter = Letter::Punct('a');

        assert!(!letter.is_upper());
        assert!(letter.is_lower());
        assert!(!letter.is_cp());
        assert!(letter.is_punct());
        assert!(letter.cp().is_none());
    }

    #[test]
    fn cp() {
        let letter = Letter::Cp(Case::Upper, 0);

        assert!(letter.is_upper());
        assert!(!letter.is_lower());
        assert!(letter.is_cp());
        assert!(!letter.is_punct());
        assert!(letter.cp().is_some());
        assert!(letter.cp() == Some(0));
    }
}
