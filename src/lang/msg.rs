use crate::prelude::*;
use std::{
    ops::{Add, Index, IndexMut, Sub},
    slice::SliceIndex,
};

use self::util::combinators::{Idx, PadTo};

#[derive(Debug, Clone, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Msg {
    pub alphabet_len: usize,
    pub data: Vec<i32>,
}

impl Msg {
    pub fn map(mut self, mut f: impl FnMut(i32) -> i32) -> Self {
        self.data.iter_mut().for_each(|cp| *cp = f(*cp));
        self
    }

    pub fn check(&self) -> bool {
        self.iter()
            .all(|i| (0..self.alphabet_len as i32).contains(&i))
    }

    pub fn iter(&self) -> impl Iterator<Item = i32> + '_ {
        self.into_iter()
    }

    pub fn len(&self) -> usize {
        self.data.len()
    }

    pub fn is_empty(&self) -> bool {
        self.data.is_empty()
    }

    pub fn period_iter(&self, period: Period, start: PeriodIdx) -> impl Iterator<Item = i32> + '_ {
        (start.0..self.len()).step_by(period.0).map(|i| self[i])
    }

    pub fn period(&self, period: Period, start: PeriodIdx) -> Self {
        Msg {
            data: self.period_iter(period, start).collect(),
            alphabet_len: self.alphabet_len,
        }
    }

    pub fn indices(&self) -> impl Iterator<Item = usize> {
        0..self.len()
    }

    pub fn indices_periodic(&self, period: Period) -> impl Iterator<Item = (usize, PeriodIdx)> {
        self.indices().map(move |i| period.column(i)).enumerate()
    }

    pub fn indices_from(&self, period: Period, start: PeriodIdx) -> impl Iterator<Item = usize> {
        (start.0..self.len()).step_by(period.0)
    }

    // Combinators

    pub fn apply<T: Into<Vec<i32>>>(self, f: impl FnOnce(Vec<i32>) -> T) -> Self {
        Self {
            alphabet_len: self.alphabet_len,
            data: f(self.data).into(),
        }
    }

    pub fn apply_try<E, T: Into<Vec<i32>>>(
        self,
        f: impl FnOnce(Vec<i32>) -> Result<T, E>,
    ) -> Result<Self, E> {
        Ok(Self {
            alphabet_len: self.alphabet_len,
            data: f(self.data)?.into(),
        })
    }

    pub fn modulo(self, alphabet_len: usize) -> Self {
        self.apply(combinators::modulo(alphabet_len as i32))
    }

    pub fn stream<F, S, Q>(self, stream: S, combine: F) -> Self
    where
        S: IntoIterator<Item = Q>,
        F: FnMut(i32, Q) -> i32,
    {
        self.apply(combinators::stream(stream, combine))
    }

    pub fn add<S, Q>(self, stream: S) -> Self
    where
        S: IntoIterator<Item = Q>,
        i32: Add<Q, Output = i32>,
    {
        self.apply(combinators::add(stream))
    }

    pub fn subtract<S, Q>(self, stream: S) -> Self
    where
        S: IntoIterator<Item = Q>,
        i32: Sub<Q, Output = i32>,
    {
        self.apply(combinators::subtract(stream))
    }

    pub fn reverse(mut self) -> Self {
        self.data.reverse();
        self
    }

    pub fn sub<F>(self, f: F) -> Self
    where
        F: FnMut(i32) -> i32,
    {
        self.apply(combinators::sub(f))
    }

    pub fn sub_slice<F>(self, f: F, group: usize) -> Self
    where
        F: FnMut(&mut [i32]),
    {
        self.apply(combinators::sub_slice(f, group))
    }

    pub fn sub_slice_exact<const N: usize, F>(self, f: F) -> Result<Self, combinators::Err>
    where
        F: FnMut([i32; N]) -> [i32; N],
    {
        self.apply_try(combinators::sub_slice_exact(f))
    }

    pub fn pad(self, to: PadTo, with: i32) -> Self {
        self.apply(combinators::pad(to, with))
    }

    pub fn pad_with<F>(self, to: PadTo, f: F) -> Self
    where
        F: FnMut(Idx) -> i32,
    {
        self.apply(combinators::pad_with(to, f))
    }

    pub fn periodic_with<F, G>(self, period: Period, f: F) -> Self
    where
        F: FnMut(PeriodIdx) -> G,
        G: FnMut(Vec<i32>) -> Vec<i32>,
    {
        self.apply(combinators::periodic_with(period, f))
    }

    pub fn periodic<C, F>(self, period: Period, choose: C) -> Self
    where
        C: FnMut(PeriodIdx) -> F + Clone,
        F: FnMut(i32) -> i32,
    {
        self.apply(combinators::periodic(period, choose))
    }

    pub fn periodic_with_last<C, F>(self, period: Period, choose: C) -> Self
    where
        C: FnMut(PeriodIdx) -> F,
        F: FnMut(i32, Idx, Option<i32>) -> i32,
    {
        self.apply(combinators::periodic_with_last(period, choose))
    }
}

impl<I> Index<I> for Msg
where
    I: SliceIndex<[i32]>,
{
    type Output = <Vec<i32> as Index<I>>::Output;

    fn index(&self, index: I) -> &Self::Output {
        self.data.index(index)
    }
}

impl<I> IndexMut<I> for Msg
where
    I: SliceIndex<[i32]>,
{
    fn index_mut(&mut self, index: I) -> &mut Self::Output {
        self.data.index_mut(index)
    }
}

impl AsRef<[i32]> for Msg {
    fn as_ref(&self) -> &[i32] {
        &self.data
    }
}

impl AsMut<Vec<i32>> for Msg {
    fn as_mut(&mut self) -> &mut Vec<i32> {
        self.data.as_mut()
    }
}

impl IntoIterator for Msg {
    type Item = i32;
    type IntoIter = std::vec::IntoIter<i32>;

    fn into_iter(self) -> Self::IntoIter {
        self.data.into_iter()
    }
}

impl<'msg> IntoIterator for &'msg Msg {
    type Item = i32;
    type IntoIter = std::iter::Copied<std::slice::Iter<'msg, i32>>;

    fn into_iter(self) -> Self::IntoIter {
        self.data.as_slice().iter().copied()
    }
}
