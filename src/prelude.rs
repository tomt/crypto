//! Symbols that are useful for implementing ciphers.

pub use crate::cipher::{Cipher, Symmetric};
pub use crate::lang::{
    context::{Context, Identity},
    letter::{Case, Letter},
    msg::Msg,
    text_map::{Letters, TextMap},
    Lang,
};
pub use crate::util::{
    self,
    combinators::{self, Period, PeriodIdx},
    modulo,
    stats::{
        self,
        StatsSize::{self, Bigrams, Quadgrams, Trigrams, Unigrams},
    },
};

pub fn default<T: Default>() -> T {
    T::default()
}
