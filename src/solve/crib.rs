//! An attack that takes a crib as a hint.

use crate::prelude::*;

/// Implemented by ciphers that can reconstruct the entire key from
/// a crib. Necessary for the blanket impl of `CribAttack`.
pub trait CribMatches: Sized {
    /// Reconstructs the key from a valid crib/ciphertext pair.
    /// `None` if the pairing is invalid.
    fn reconstruct_key(lang: &Lang, pos: usize, crib: &[i32], ciphertext: &[i32]) -> Option<Self>;
}

/// A cipher that can be solved with the aid of a crib.
pub trait CribAttack {
    /// Implementation-defined options for the solving algorithm.
    type Options;

    /// Solves the ciphertext using a crib or multiple cribs.
    fn crib_attack<'cribs>(
        ctx: &Context,
        lang: &Lang,
        msg: &str,
        cribs: Cribs<'cribs>,
        options: Self::Options,
    ) -> Self;
}

impl<C> CribAttack for C
where
    C: Cipher + CribMatches,
{
    type Options = StatsSize;

    fn crib_attack<'cribs>(
        ctx: &Context,
        lang: &Lang,
        msg: Msg,
        cribs: Cribs<'cribs>,
        stats_size: Self::Options,
    ) -> Self {
        let ciphertext = alph.code_points(msg).collect::<Vec<i32>>();
        let mut best_cipher = None;
        let mut best_score = f32::MIN;

        for crib in cribs {
            let plain = alph.code_points(crib.plaintext()).collect::<Vec<i32>>();
            if plain.is_empty() || plain.len() > ciphertext.len() {
                continue;
            }

            let mut eval_pos = |pos| {
                let cipher = &ciphertext[pos..][..plain.len()];

                if let Some(cipher) = C::reconstruct_key(lang, pos, &plain, cipher) {
                    // let score = crib.priority().multiplier() as f32
                    //     * alph.score_str(&cipher.decrypt(msg), stats_size);
                    let score = todo!();

                    if score > best_score {
                        best_score = score;
                        best_cipher = Some(cipher);
                    }
                }
            };

            if let Some(pos) = crib.pos() {
                eval_pos(pos);
            } else {
                for pos in 0..ciphertext.len() - plain.len() {
                    eval_pos(pos);
                }
            }
        }

        best_cipher.unwrap_or_else(|| C::default(lang))
    }
}

/// The priority of a crib.
#[derive(Debug, Clone, Copy)]
pub enum Priority {
    /// Part of the crib is likely to be in the plaintext.
    Partial,
    /// Most or all of the crib is likely to be in the plaintext.
    Likely,
    /// The entire crib is almost certainly in the plaintext.
    Certain,
}

impl Priority {
    /// Gets the multiplier corresponding to the priority, higher priority
    /// means a higher multiplier.
    pub fn multiplier(&self) -> usize {
        match self {
            Priority::Partial => 1,
            Priority::Likely => 2,
            Priority::Certain => 3,
        }
    }
}

/// A portion of known plaintext.
#[derive(Debug)]
pub struct Crib<'crib> {
    pos: Option<usize>,
    guessed: &'crib str,
    priority: Priority,
}

impl<'crib> Crib<'crib> {
    /// Gets the crib position.
    pub fn pos(&self) -> Option<usize> {
        self.pos
    }
    /// Gets the crib text.
    pub fn plaintext(&self) -> &'crib str {
        self.guessed
    }
    /// Gets the crib priority.
    pub fn priority(&self) -> Priority {
        self.priority
    }
}

/// Multiple portions of known plaintext.
#[derive(Debug, Default)]
pub struct Cribs<'cribs> {
    cribs: Vec<Crib<'cribs>>,
}

impl<'cribs> Cribs<'cribs> {
    /// Add a crib to the collection.
    pub fn crib(mut self, guessed: &'cribs str, pos: Option<usize>, priority: Priority) -> Self {
        self.cribs.push(Crib {
            pos,
            guessed,
            priority,
        });
        self
    }
}
impl<'a> IntoIterator for Cribs<'a> {
    type Item = Crib<'a>;
    type IntoIter = std::vec::IntoIter<Crib<'a>>;

    fn into_iter(self) -> Self::IntoIter {
        self.cribs.into_iter()
    }
}
