//! Breaking, encrypting and decrypting periodic (polyalphabetic) ciphers.

use crate::prelude::*;
use rayon::prelude::*;
use std::cmp::Ordering;

use self::util::combinators::Period;

use super::substitution;

/// Uses the chi-squared statistic to solve a classic vigenere cipher with
/// a known period. Returns the score and solution.
pub fn solve_simple_with_period(
    ciphertext: &Msg,
    lang: &Lang,
    stats_size: StatsSize,
    period: usize,
) -> (f32, Vec<i32>) {
    let len = ciphertext.alphabet_len as i32;
    let mut key = vec![0; period];

    for (i, col) in key.iter_mut().enumerate() {
        let mut min_chi_sq = f32::MAX;

        for shift in 0..len {
            let chi_sq = lang.chi_squared(
                (i..ciphertext.len())
                    .step_by(period)
                    .map(|idx| modulo(ciphertext[idx] - shift, len)),
            );

            if chi_sq < min_chi_sq {
                min_chi_sq = chi_sq;
                *col = shift;
            }
        }
    }

    let score = lang.score(
        ciphertext
            .iter()
            .enumerate()
            .map(|(i, cp)| modulo(cp - key[i % period], len)),
        stats_size,
    );

    (score, key)
}

/// Solves the vigenere cipher as a periodic caesar cipher, by
/// evaluating the chi-squared score of each slice with each
/// possible shift.
pub fn solve_simple(
    ciphertext: &Msg,
    lang: &Lang,
    stats_size: StatsSize,
    max_period: usize,
) -> (f32, Vec<i32>) {
    (1..max_period.min(ciphertext.len()))
        .into_par_iter()
        .map(|period| solve_simple_with_period(ciphertext, lang, stats_size, period))
        .max_by(cmp_solutions)
        .unwrap_or_else(|| (f32::MIN, vec![0]))
}

/// Solves a variant of vigenere cipher with a known period,
/// returning the score and best solution.
pub fn solve_vig_with_period(
    ciphertext: &Msg,
    lang: &Lang,
    stats_size: StatsSize,
    tableau: impl Fn(i32, i32, Option<i32>) -> i32,
    shift_increment: usize,
    period: Period,
) -> (f32, Vec<i32>) {
    let len = ciphertext.len();
    let mut key = vec![0; period.0];
    let mut curr_score = f32::MIN;

    let mut plaintext = Vec::with_capacity(ciphertext.len());

    // initial decrypt
    for cp in ciphertext.iter().take(period.0) {
        plaintext.push(tableau(0, cp, None));
    }
    for i in period.0..ciphertext.len() {
        plaintext.push(tableau(0, ciphertext[i], Some(plaintext[i - period.0])))
    }

    // loop until no improvement
    let mut prev_score = 0.0;
    while prev_score != curr_score {
        prev_score = curr_score;

        // try to improve each key column
        for col in period.columns() {
            // brute force shifts
            for shift in (0..len as i32).step_by(shift_increment) {
                // decrypt only the column that changed
                plaintext[col] = tableau(shift, ciphertext[col.0], None);
                for i in ciphertext.indices_from(period, col).skip(1) {
                    plaintext[i] = tableau(shift, ciphertext[i], Some(plaintext[i - period.0]));
                }

                let score = lang.score(&plaintext, stats_size);

                if score > curr_score {
                    curr_score = score;
                    key[col] = shift;
                }
            }

            // decrypt again using the best shift
            plaintext[col] = tableau(key[col], ciphertext[col.0], None);
            for i in ciphertext.indices_from(period, col).skip(1) {
                plaintext[i] = tableau(key[col], ciphertext[i], Some(plaintext[i - period.0]));
            }
        }
    }

    (curr_score, key)
}

/// Solves any variant of the vigenere cipher, provided the cipher
/// `alph`abet, a function which performs the decryption (`tableau`),
/// the increment that should be used for shifts, the maximum key
/// length that should be tested (exclusive), and the `stats_size`
/// used to score a plaintext.
///
/// `tableau` is a function of signature: \
/// (`key_cp`, `ciphertext_cp`, `Option<plaintext_cp>`) -> `plaintext_cp` \
/// where `Option<plaintext_cp>` is the most recent decryption in the same
/// column (which may be `None`), used for the autokey cipher.
pub fn solve_vig(
    ciphertext: &Msg,
    lang: &Lang,
    stats_size: StatsSize,
    tableau: impl Fn(i32, i32, Option<i32>) -> i32 + Send + Sync + Copy,
    shift_increment: usize,
    max_period: usize,
) -> (f32, Vec<i32>) {
    (1..max_period.min(ciphertext.len() / 2))
        .into_par_iter()
        .map(Period)
        .filter(|&period| period.is_likely(ciphertext))
        .map(|period| {
            solve_vig_with_period(
                ciphertext,
                lang,
                stats_size,
                tableau,
                shift_increment,
                period,
            )
        })
        .max_by(cmp_solutions)
        .unwrap_or_else(|| (f32::MIN, vec![0]))
}

/// Compares two solutions to the vigenere cipher, as (score, key) tuples.
/// This priorities keys of lesser length where the scores are the same.
fn cmp_solutions<T>(a: &(f32, Vec<T>), b: &(f32, Vec<T>)) -> Ordering {
    match a.0.partial_cmp(&b.0).unwrap() {
        Ordering::Equal => a.1.len().cmp(&b.1.len()).reverse(),
        ord => ord,
    }
}

/// Solves a cipher in which letters in each period slice are enciphered
/// by a monoalphabetic substitution with a known period. Returns the
/// score and the substitution key for each period.
pub fn solve_periodic_polyalphabetic_with_period(
    ciphertext: &Msg,
    lang: &Lang,
    stats_size: StatsSize,
    period: Period,
) -> (f32, Vec<Vec<i32>>) {
    let len = ciphertext.alphabet_len;
    let mut plaintext = vec![0; ciphertext.len()];

    let mut parent_key: Vec<Vec<usize>> = vec![];

    for start in period.columns() {
        let column = ciphertext.period(period, start);
        let sub = substitution::solve_statistical(&column, lang);
        let inv = util::invert(&sub);

        parent_key.push(inv.into_iter().map(|x| x as usize).collect());
    }

    // decrypt with the parent key.
    for (idx, period_idx) in ciphertext.indices_periodic(period) {
        plaintext[idx] = parent_key[period_idx.0][ciphertext[idx] as usize];
    }

    let mut parent_score;
    let mut best_score = lang.score(ciphertext, stats_size);
    let mut best_key: Vec<Vec<usize>> = period.columns().map(|_| (0..len).collect()).collect();

    // outer loop
    let mut big_counter = 0;
    while big_counter < period.0.pow(2) * 1_000 {
        // try each alphabet
        for col in period.columns() {
            fastrand::shuffle(&mut parent_key[col]);

            // update this period of the plaintext.
            for idx in ciphertext.indices_from(period, col) {
                plaintext[idx] = parent_key[col][ciphertext[idx] as usize];
            }

            parent_score = lang.score(&plaintext, stats_size);

            loop {
                let mut improved = false;

                for a in 0..len {
                    for b in 0..len - 1 {
                        let b = if a == b { b + 1 } else { b };

                        parent_key[col].swap(a, b);
                        // update this period of the plaintext.
                        for idx in ciphertext.indices_from(period, col) {
                            plaintext[idx] = parent_key[col][ciphertext[idx] as usize];
                        }

                        let score = lang.score(&plaintext, stats_size);

                        if score > parent_score {
                            parent_score = score;
                            improved = true;
                        } else {
                            parent_key[col].swap(a, b);
                        }

                        if score > best_score {
                            best_score = score;
                            best_key[col].copy_from_slice(&parent_key[col]);
                            big_counter = 0;
                        }

                        big_counter += 1;
                    }
                }

                if !improved {
                    break;
                }
            }

            // update this period of the plaintext.
            for i in ciphertext.indices_from(period, col) {
                plaintext[i] = parent_key[col][ciphertext[i] as usize];
            }
        }
    }

    let best_key = best_key
        .into_iter()
        .map(|x| util::invert(&x).into_iter().map(|y| y as i32).collect())
        .collect();

    (best_score, best_key)
}

/// Solves a cipher in which letters in each period slice are enciphered
/// by a monoalphabetic substitution. Returns the score and the substitution
/// key for each period.
pub fn solve_periodic_polyalphabetic(
    ciphertext: &Msg,
    lang: &Lang,
    stats_size: StatsSize,
    max_period: Period,
) -> (f32, Vec<Vec<i32>>) {
    let len = ciphertext.alphabet_len as i32;

    (1..max_period.0.min(ciphertext.len() / 20))
        .into_par_iter()
        .map(Period)
        .filter(|&period| period.is_likely(ciphertext))
        .map(|period| {
            solve_periodic_polyalphabetic_with_period(ciphertext, lang, stats_size, period)
        })
        .max_by(cmp_solutions)
        .unwrap_or_else(|| (f32::MIN, vec![(0..len).collect()]))
}
