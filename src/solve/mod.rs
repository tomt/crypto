use crate::prelude::*;

pub mod brute_force;
// pub mod crib;
// pub mod dictionary;
pub mod periodic;
pub mod substitution;
// pub mod transposition;
pub mod vigenere;

/// Solve a ciphertext using an arbitrary algorithm that can be configured.
pub trait Solve {
    /// Used to configure the solving algorithm.
    type Options;

    /// Solves a message using an arbitrary algorithm and options.
    fn solve(ctx: &Context, lang: &Lang, msg: &Msg, options: Self::Options) -> Self;
}
