//! Brute force attack on a cipher. This simply evaluates all possible
//! solutions, returning the best.

use crate::prelude::*;
use rayon::prelude::*;

/// Implemented by ciphers to provide support for brute forcing.
/// Defines a function that returns an iterator over the complete
/// key space of the cipher.
pub trait KeySpace {
    /// The iterator type for the key space.
    type Iter: Iterator<Item = Self>;
    /// Restrict the region of key space that is generated.
    type Limit;

    /// Gets an iterator over the key space of the cipher.
    fn key_space(ctx: &Context, limit: Self::Limit) -> Self::Iter;
}

/// A cipher that can be solved through searching the full keyspace.
///
/// Any cipher, `C: Cipher + KeySpace + Send` has a blanket impl
/// which traverses the key space and returns the cipher associated with
/// the highest scoring plaintext.
pub trait BruteForce {
    /// Options to configure the brute force search.
    type Options;

    /// Brute forces the cipher, returning the optimal solution.
    fn brute_force(ctx: &Context, lang: &Lang, msg: Msg, options: Self::Options) -> Self;
}

impl<C> BruteForce for C
where
    C: Send + Cipher + KeySpace + Identity,
    C::Iter: Send,
{
    type Options = BruteForceOptions<C::Limit>;

    fn brute_force(ctx: &Context, lang: &Lang, msg: Msg, options: Self::Options) -> Self {
        C::key_space(ctx, options.limit)
            .take(options.max_iterations.unwrap_or(usize::MAX))
            .par_bridge()
            .map(|cipher| {
                let plaintext = cipher.decrypt(ctx, msg.clone()).unwrap();
                let score = lang.score(plaintext, options.stats_size);

                (score, cipher)
            })
            .max_by(|(score1, _), (score2, _)| score1.partial_cmp(score2).unwrap())
            .map(|(_, cipher)| cipher)
            .unwrap_or_else(|| C::identity(ctx))
    }
}

/// Options for configuring the default brute force implementation.
#[derive(Debug, Default)]
pub struct BruteForceOptions<L> {
    /// [`StatsSize`] for scoring.
    pub stats_size: StatsSize,
    /// Maximum number of cipher keys to try.
    pub max_iterations: Option<usize>,
    /// Implementation dependent parameter for [`KeySpace::key_space`].
    pub limit: L,
}

/// The maximum period length for generating the key space of
/// polyalphabetic ciphers.
#[derive(Debug)]
pub struct MaxPeriod(pub usize);

impl Default for MaxPeriod {
    fn default() -> Self {
        MaxPeriod(4)
    }
}
