//! Attacks on monoalphabetic substitution ciphers.

use crate::prelude::*;
use std::cmp::Ordering;

/// Solves a monoalphabetic substitution statistically, using sorted
/// letter frequencies.
pub fn solve_statistical(ciphertext: &Msg, lang: &Lang) -> Vec<i32> {
    let len = ciphertext.alphabet_len as i32;
    let mut expected_freqs: Vec<(f32, i32)> = lang
        .stats(Unigrams)
        .frequencies_iter()
        .zip(0..len)
        .collect();

    expected_freqs.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap());

    let actual_freqs = stats::frequencies(&stats::counts(&ciphertext.data, StatsSize::Unigrams));
    let mut actual_freqs: Vec<_> = actual_freqs.into_iter().zip(0..len).collect();

    actual_freqs.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap());

    let mut solution = vec![0; len as usize];

    for ((_, plain), (_, cipher)) in expected_freqs.into_iter().zip(actual_freqs) {
        solution[plain as usize] = cipher;
    }

    solution
}

/// Solves a monoalphabetic substitution using hill climbing.
/// `cp` is a slice of ciphertext codepoints. Returns the
/// substitution key.
pub fn solve_hill_climbing(
    ciphertext: &Msg,
    lang: &Lang,
    max_iterations: usize,
    min_repetitions: usize,
    stats_size: StatsSize,
) -> (f32, Vec<i32>) {
    let len = ciphertext.alphabet_len as i32;
    let mut best_score = f32::MIN;
    let mut best_inv: Vec<i32> = (0..len).collect();
    let mut repetitions = 0;

    for _ in 0..max_iterations {
        let mut local_score = f32::MIN;
        let mut local_inv: Vec<i32> = (0..len).collect();

        fastrand::shuffle(&mut local_inv);

        // keep trying all possible swaps until there is no further improvement
        loop {
            let mut improved = false;

            for i in 0..len as usize {
                for j in (0..len as usize).filter(|&j| j != i) {
                    local_inv.swap(i, j);

                    let score =
                        lang.score(ciphertext.iter().map(|x| local_inv[x as usize]), stats_size);

                    if score > local_score {
                        local_score = score;
                        improved = true;
                    } else {
                        local_inv.swap(i, j);
                    }
                }
            }

            if !improved {
                break;
            }
        }

        use Ordering::*;
        match local_score.partial_cmp(&best_score).unwrap() {
            Equal => repetitions += 1,
            Greater => {
                repetitions = 0;
                best_score = local_score;
                best_inv = local_inv;
            }
            Less => (),
        }

        if repetitions >= min_repetitions {
            break;
        }
    }

    (best_score, util::invert(&best_inv))
}
