//! Utilities for transposition [`Cipher`](crate::cipher::Cipher)s.

use crate::prelude::*;
use rayon::prelude::*;

/// Trait implemented by transposition ciphers, which permits encryption
/// and decryption.
pub trait Transposition {
    /// Stores the positions of the plaintext code points within a ciphertext
    /// of length `len`, provided the cipher settings. `buf` has length >= `len`.
    /// Each value must be unique and in the range `0..len`.
    fn decrypt_indices(&self, len: usize, buf: &mut Vec<usize>);
    /// Stores the positions of the ciphertext code points within a plaintext
    /// of length `len`, provided the cipher settings. `buf` has length >= `len`.
    /// Each value must be unique and in the range `0..len`. This function has
    /// a default implementatin by inverting the result of `decrypt_indices`, however
    /// this will allocate, and since `decrypt_indices` may be used for solving, it
    /// is preffered that `decrypt_indices` has the most efficient implementation.
    fn encrypt_indices(&self, len: usize, buf: &mut Vec<usize>) {
        let mut key = vec![0; len];
        self.decrypt_indices(len, &mut key);

        // inverse transformation can be automatically determined.
        util::invert_into(&key, buf);
    }
}

fn transpose<F>(msg: &str, alph: WithAlphabet<'_>, transformation: F) -> String
where
    F: Fn(usize, &mut Vec<usize>),
{
    let code_points: Vec<_> = alph.code_points(msg).collect();
    let len = code_points.len();

    let mut key = vec![0; len];
    transformation(len, &mut key);
    let mut key_iter = key.into_iter();

    alph.letters(msg)
        .map_cp(alph, |_| code_points[key_iter.next().unwrap()])
        .chars(alph)
        .collect()
}

/// Performs a transposition on the letters in `msg`, provided the new positions
/// of each item in `msg` from [`Transposition::encrypt_indices`].
pub fn encrypt(msg: &str, alph: WithAlphabet<'_>, t: &impl Transposition) -> String {
    transpose(msg, alph, |len, buf| t.encrypt_indices(len, buf))
}

/// Performs a transposition on the letters in `msg`, provided the new positions
/// of each item in `msg` from [`Transposition::decrypt_indices`].
pub fn decrypt(msg: &str, alph: WithAlphabet<'_>, t: &impl Transposition) -> String {
    transpose(msg, alph, |len, buf| t.decrypt_indices(len, buf))
}

/// Solves a block/column transposition.
///
/// * `decrypt_indexes` A function of type: (len, key_order) -> decrypt_indexes
/// * `get_index` A function of type: (row, col, key_len, num_rows) -> index
pub fn transposition_solve<F, G>(
    ciphertext: &[i32],
    alph: &WithAlphabet<'_>,
    decrypt_indexes: F,
    get_index: G,
) -> Vec<i32>
where
    F: Fn(usize, Vec<usize>) -> Vec<usize> + Send + Sync + Copy,
    G: Fn(usize, usize, usize, usize) -> usize + Send + Sync + Copy,
{
    let len = ciphertext.len();
    let bigrams = alph.stats(StatsSize::Bigrams);

    // try all key lengths
    (3.min(len)..15.min(len))
        .into_par_iter()
        .flat_map(|key_len| {
            // ignore any extra above a multiple of key_len
            let num_rows = len / key_len;

            (0..key_len).into_par_iter().map(move |start_col| {
                let mut key = vec![start_col];

                while key.len() < key_len {
                    let col1 = key.last().copied().unwrap();

                    let mut max_score = f32::MIN;
                    let mut max_col = 0;

                    for col2 in (0..key_len).filter(|x| !key.contains(x)) {
                        let mut total_score = 0.0;

                        for row in 0..num_rows {
                            let col1_letter = ciphertext[get_index(row, col1, key_len, num_rows)];
                            let col2_letter = ciphertext[get_index(row, col2, key_len, num_rows)];

                            total_score += bigrams[((col1_letter << 5) | col2_letter) as usize];
                        }

                        if total_score > max_score {
                            max_score = total_score;
                            max_col = col2;
                        }
                    }

                    key.push(max_col);
                }

                let score = alph.score(
                    decrypt_indexes(len, key.clone())
                        .into_iter()
                        .map(|idx| ciphertext[idx] as usize),
                    StatsSize::Quadgrams,
                );

                (score, key)
            })
        })
        .max_by(|(score1, _), (score2, _)| score1.partial_cmp(score2).unwrap())
        .map(|(_, key)| key.into_iter().map(|x| x as i32).collect())
        .unwrap_or_else(|| vec![0])
}
