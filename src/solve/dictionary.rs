//! An attack that uses a list of keywords to generate keys to try.

use crate::{cipher::Cipher, lang::Lang, util::stats::StatsSize};
use rayon::prelude::*;

/// Ciphers that can created from a keyword. There may be multiple
/// interpretations of a keyword through which a cipher can be created,
/// so the possibilities are returned as an iterator.
///
/// When this trait is implemented for a cipher, the `DictionaryAttack`
/// trait may be implemented by a blanket impl.
pub trait FromKeyword<'l, 'kw> {
    /// Iterator over the possible ciphers that could be constructed
    /// from the keyword.
    type Iter: Iterator<Item = Self>;
    /// Set the variants of cipher that the implementation will generate.
    /// E.g. FillStrategy for the substitution cipher.
    type Variants;

    /// Gets an iterator over the cipher variants that can be constructed from
    /// the keyword.
    fn from_keyword(lang: &'l Lang, keyword: &'kw str, variants: Self::Variants) -> Self::Iter;
}

/// A cipher that can be solved by trying a list of keywords.
pub trait DictionaryAttack<'l, 'kw> {
    /// Implementation-defined options for the solving algorithm.
    type Options;

    /// Solves the ciphertext using keywords in a dictionary.
    fn dictionary_attack(
        lang: &'l Lang,
        msg: &str,
        keywords: impl Iterator<Item = &'kw str> + Send,
        options: Self::Options,
    ) -> Self;
}

impl<'l, 'kw, C> DictionaryAttack<'l, 'kw> for C
where
    C: Send + Cipher + FromKeyword<'l, 'kw>,
    C::Iter: Send,
    C::Variants: Default,
{
    type Options = StatsSize;

    fn dictionary_attack(
        lang: &'l Lang,
        msg: &str,
        keywords: impl Iterator<Item = &'kw str> + Send,
        stats_size: Self::Options,
    ) -> Self {
        let alph = lang.with_alphabet(AlphabetLen::Primary);
        keywords
            .flat_map(|kw| C::from_keyword(lang, kw, C::Variants::default()))
            .par_bridge()
            .map(|cipher| {
                let plaintext = cipher.decrypt(msg);
                let score = alph.score_str(&plaintext, stats_size);
                (score, cipher)
            })
            .max_by(|(score1, _), (score2, _)| score1.partial_cmp(score2).unwrap())
            .map(|(_, cipher)| cipher)
            .unwrap_or_else(|| C::default(lang))
    }
}
