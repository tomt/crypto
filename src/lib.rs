#![doc = include_str!("../README")]
#![forbid(unsafe_code)]
// #![deny(missing_docs)]

pub mod cipher;
pub mod lang;
pub mod prelude;
pub mod solve;
pub mod util;

#[cfg(test)]
mod common {
    use crate::prelude::*;
    use nanoserde::{DeBin, SerBin};

    pub fn encrypt_str<C: Cipher>(cipher: &C, text: &str) -> String {
        let (templates, msg) = TEXT_MAP.letters(text).split();

        let ciphertext = cipher.encrypt(CONTEXT, msg).unwrap();
        let ciphertext = Letters::join(templates, ciphertext).unwrap();

        TEXT_MAP.to_str(ciphertext)
    }

    pub fn decrypt_str<C: Cipher>(cipher: &C, text: &str) -> String {
        let (templates, msg) = TEXT_MAP.letters(text).split();

        let ciphertext = cipher.decrypt(CONTEXT, msg).unwrap();
        let ciphertext = Letters::join(templates, ciphertext).unwrap();

        TEXT_MAP.to_str(ciphertext)
    }

    pub fn test_encrypt_decrypt<C: Cipher>(cipher: &C, plaintext: &str, ciphertext: &str) {
        assert_eq!(encrypt_str(cipher, plaintext), ciphertext);
        assert_eq!(decrypt_str(cipher, ciphertext), plaintext);
    }

    pub static CONTEXT: &Context = &Context { alphabet_len: 26 };

    const LANG_FILE_DIR: &str = "data/lang";
    const LANG_FILE_PATH: &str = "data/lang/english.lang";

    lazy_static::lazy_static! {
        pub static ref TEXT_MAP: TextMap = {
            TextMap::infer("")
        };

        pub static ref LANG: Lang = {
            if let Ok(bytes) = std::fs::read(LANG_FILE_PATH) {
                Lang::deserialize_bin(&bytes).unwrap()
            } else {
                let corpus = std::fs::read_to_string("data/corpus/alice.txt").unwrap();
                let text_map = TextMap::infer(&corpus);

                let (_, corpus_cp) = text_map.letters(&corpus).split();
                let lang = Lang::new(corpus_cp);

                let bytes = lang.serialize_bin();

                std::fs::create_dir_all(LANG_FILE_DIR).unwrap();
                std::fs::write(LANG_FILE_PATH, bytes).unwrap();

                lang
            }
        };
    }
}
